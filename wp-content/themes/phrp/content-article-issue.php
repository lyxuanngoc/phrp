<?php
/**
 * Get all articles for the issue. We need to use a custom SQL here as opposed to just a WPQuery with args because of the custom sort order
 * required which is article type, then sorted by the menu order.
 */

// If is issue, sort by menu order
$isIssue = get_field('is_issue');
$sortByMenuOrder = ($isIssue === "Yes") ? 'p.menu_order ASC, ' :'';
$isThemed = get_field('is_themed');
$description_themed = get_field('description_themed');
$sql = "SELECT
          *
        FROM
          wp_posts p
          LEFT JOIN wp_postmeta c ON (p.ID = c.post_id)
        WHERE p.post_type = 'article'
          AND p.post_status = 'publish'
          AND p.post_parent = ". $post->ID ."
          AND c.meta_key = 'article_type'
          ORDER BY FIELD(c.meta_value,'" . implode("','", array_values(array_keys(IssueArticle::$articleTypes))) . "'), ".$sortByMenuOrder." p.post_date DESC;";

global $wpdb;
$articles = $wpdb->get_results($sql);

$issueType = get_field('issue_type');

//gets the featured article for the issue, only applicable to the current issue.
$featuredArticle = get_field('featured_article');

?>

<div class="current_issue">
    <?php if($issueType == 'Current Issue'){
        $headingText = $issueType . ' - ' . the_title('','',false);
    }
    else{
        $headingText = the_title('','',false);
    }
    ?>

    <h1><?php echo $headingText; ?></h1>
    <!-- add themed -->
    <?php if($isThemed && $isIssue === "Yes" && !empty($description_themed)) : ?>
    <div class="themed">
      <div class="flag-themed">THEME</div>
      <p><?php echo $description_themed ?></p>
    </div>
    <?php endif; ?>
    <?php


    //If we should show a featured article for this issue
    if($featuredArticle){

        ?><h2 class="featured_article">Editorial</h2><?php

        IssueArticle::article_snippet($featuredArticle,'featured_article');

    }

    $currentArticleType = '';

    foreach($articles as $article){

        //Don't show the featured article here since it's already being shown at the top
        if($featuredArticle && $featuredArticle->ID == $article->ID){
            continue;
        }
        /**
         * Do we have a new article type to display?
         */
        if (get_field('article_type',$article->ID) !== $currentArticleType) {
            $currentArticleType = get_field('article_type',$article->ID);

            $currentArticleTypeData = get_field_object('article_type',$article->ID);

            //get the label for display purposes
            $articleLabel = ucfirst(strtolower($currentArticleTypeData['choices'][$currentArticleType]));

            //dont show the sub-section if we're looking at online first or old articles.
            if(!in_array($currentArticleType,array('online_early','nsw_public_health_bulletin_archive'))) {

                ?> <h2 class="<?php echo $currentArticleType; ?>"><?php echo ucwords($articleLabel); ?></h2> <?php
            }

        }

        IssueArticle::article_snippet($article,$currentArticleType);

    } ?>

</div>