<?php

/**
 * Outputs tiles ACF for the current page
 */

$editorialBios = new EditorialBios();
$groups = $editorialBios->get();

if ($groups) {
    $groupCount = 0;
    $groupName = '';

    //initial div
    foreach ($groups as $group => $member) {
        $count = 0;
        $groupCount++;
        ?>
            <?php if ($groupCount > 1) { ?>
                 <h1><?php echo ucfirst(str_replace('-', ' ', $group)); ?></h1>
            <?php } ?>

            <?php if (strtolower($group) === EditorialBios::INTERNATIONAL_GROUP) { ?>
                <p>The PHRP International panel is an adjunct to the Editorial board: it provides an international
                    perspective to local and national public health issues, and ensures that the journal is connected
                    to a broader international community of public health research and practice.</p>
            <?php } ?>

            <div class="row bio-rows">

            <?php foreach ($member['bios'] as $bio) { ?>

                <div class="team_member <?php echo($bio['photo'] ?: 'noimage'); ?>">
                    <div class="col-xs-12 col-lg-4">
                        <h5><?php echo $bio['position']; ?></h5>

                        <?php if ($bio['photo']) {?>
                            <img src="<?php echo $bio['photo']['url'] ?>" alt="<?php echo $bio['photo']['alt']; ?>" class="img-responsive"/>
                        <?php } ?>

                        <h4><?php echo $bio['first_name'] . ' ' . $bio['last_name']; ?></h4>

                        <p>
                            <?php echo $bio['biography']; ?>
                        </p>
                    </div>
                </div>

                <?php
                //echo new row when it's finishing outputting the THIRD item.
                if (++$count % 3 == 0) {
                    echo '</div><div class="row">';
                }
            }
            ?>

            </div>

        <?php
    }
}

?>
