<?php

require_once 'libs/EditorialBios.php';
require_once 'libs/acf/accordion-template.php';

//add ACF options page
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();

}

function seopress_theme_slug_setup() {
    add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'seopress_theme_slug_setup' );

add_action('wp_enqueue_scripts', function () {
    /**
     * Styles
     */
    $pathToCSS = __DIR__.'/less/style.css';
    $cssVersion = file_exists($pathToCSS) ? filemtime($pathToCSS) : null;


    wp_enqueue_style('font-awesome', get_stylesheet_directory_uri() . '/css/font-awesome.min.css');
    wp_enqueue_style('phrp-style', get_stylesheet_directory_uri() . '/less/style.css', array('bootstrap.css'), $cssVersion);
    wp_enqueue_style('phrp-raw-style', get_stylesheet_directory_uri() . '/css/raw.css', [], '1.0.3');

    /**
     * Scripts
     */
    wp_enqueue_script('accordions', get_stylesheet_directory_uri() . '/js/accordions.js', array('jquery'), null, true);

	wp_deregister_script('jquery');
	wp_register_script('jquery', 'https://code.jquery.com/jquery-2.2.4.min.js');

});

/**
 * Return latest issue articles that are published.
 *
 * @return array
 */
function latestIssueArticles()
{
    $nextPublishDateTime = getNextIssuePublishDateTime();

    $dateTime = new \DateTime();
    $dateTime->setTimezone(new \DateTimeZone('Australia/Sydney'));
    $currentDate = $dateTime->format('Y-m-d H:i');

    // If today is the day of future publish date, then start updating the current
    // tiles with the new ones. Afterwards clear out the future publish date
    // and clear out the future tiles so there isn't another update.
    if ($nextPublishDateTime && $currentDate >= $nextPublishDateTime) {
        updateIssueArticles();
    }

    return get_field('homepage_tiles', 'option');
}

/**
 * Return the next issue publish dateTime if the set date is not empty.
 *
 * @return string
 */
function getNextIssuePublishDateTime()
{
    $nextIssuePublishDateTime = get_field('next_issue_publish_date_time', 'option');

    $dateTime = $nextIssuePublishDateTime[0];

    if (!empty($dateTime['date'])) {
        return $dateTime['date'].' '.$dateTime['hours'].':'.$dateTime['minutes'];
    }

    return '';
}

/**
 * Update the issue articles if there are any set.
 */
function updateIssueArticles()
{
    $homepageNextTiles = get_field('homepage_new_tiles', 'option');

    if ($homepageNextTiles) {
        // Replace the current issue titles with the new ones
        update_field('homepage_tiles', $homepageNextTiles, 'option');

        // Clear out the future set article tiles
        update_field('homepage_new_tiles', array(), 'option');

        // Clear out the next issue date so we don't update the next time the page loads
        update_field('next_issue_publish_date_time', array(), 'option');
    }
}

/**
 * Check if issue is published.
 *
 * @param string|integer $post
 * @return bool
 */
function isIssuePublished($post)
{
    $postId = (is_numeric($post)) ? $post : url_to_postid($post);
    $issueStatus = get_post_status($postId);

    return (!empty($issueStatus) && $issueStatus == 'publish') ? true : false;
}

/**
 * Return slides. Exclude slides if the slide is linked to an internal issue and is not published yet.
 *
 * @return array
 */
function homePageSlider()
{
    $homepageSlider = (get_field('promo_image_slider')) ? get_field('promo_image_slider') : array();

    if (empty($homepageSlider)) {
        return $homepageSlider;
    }

    $homepageSlider = array_filter($homepageSlider, function($value) {
        return (($value['link_type'] == 'internal' && isIssuePublished($value['internal_link'])) || $value['link_type'] == 'external');
    });

    return (is_array($homepageSlider) && !empty($homepageSlider)) ? $homepageSlider : array();
}

/**
 * Re-implements the "get shortlinks" button in the post add/edit page.
 * This feature was hidden by default since Wordpress v4.4
 */
add_filter('get_shortlink', function($shortlink) {
    return $shortlink;
});

add_filter('xmlrpc_enabled', '__return_false');

//@@BM
add_image_size( 'in-evidence', 720, 480, true );
add_image_size( 'issue', 225, 145, true );
add_image_size( 'issue-hp', 245, 145, true );
add_image_size( 'issue-big', 445, 470, true );

add_filter( 'gform_submit_button_3', 'form_submit_button', 10, 2 );

function form_submit_button( $button, $form ) {
    return "<button class='btn btn-block btn-green' id='gform_submit_button_{$form['id']}'><span class='text sr-only'>Subscribe</span>
    <span class='icon'><i class='fa fa-arrow-circle-right'></i></span></button>";
}

// Remove comments completely
add_action( 'admin_init', function () {
    // Redirect any user trying to access comments page
    global $pagenow;

    if ( $pagenow === 'edit-comments.php' || $pagenow === 'options-discussion.php' ) {
        wp_redirect( admin_url() );
        exit;
    }

    // Remove comments metabox from dashboard
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );

    // Disable support for comments and trackbacks in post types
    foreach ( get_post_types() as $post_type ) {
        if ( post_type_supports($post_type, 'comments') ) {
            remove_post_type_support( $post_type, 'comments' );
            remove_post_type_support( $post_type, 'trackbacks' );
        }
    }
});

// Close comments on the front-end
add_filter( 'comments_open', '__return_false', 20, 2 );
add_filter( 'pings_open', '__return_false', 20, 2 );

// Hide existing comments
add_filter( 'comments_array', '__return_empty_array', 10, 2 );

// Remove comments page and option page in menu
add_action( 'admin_menu', function() {
    remove_menu_page( 'edit-comments.php' );
    remove_submenu_page( 'options-general.php', 'options-discussion.php' );
});

// Remove comments link in admin toolbar
function my_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'my_admin_bar_render' );

// Remove comments links from admin bar
add_action( 'init', function () {
    if ( is_admin_bar_showing() ) {
        remove_action( 'admin_bar_menu', 'wp_admin_bar_comments_menu', 60 );
    }
});
