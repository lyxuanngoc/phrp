var gulp = require("gulp");
var less = require('gulp-less');
var cleanCSS = require('gulp-clean-css');
var path = require('path');

gulp.task('less', function () {
  return gulp.src('./less/style.less')
    .pipe(less({
      paths: [ path.resolve("./less") ]
    }))
    .pipe(gulp.dest('./less'));
});

gulp.task('minifycss', function () {
  return gulp.src('./less/style.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./less'));
});

gulp.task('watch', function() {
  gulp.watch('./less/**/*.less', gulp.series('less'));
});

gulp.task('default', gulp.series([
  'less',
  'minifycss'
]));