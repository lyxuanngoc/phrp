<?php

global $wp_query;
$query =  $wp_query;
$big = 999999999;

$currentPage =  max( 1, get_query_var('paged') );

$paginate = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'type' => 'array',
        'total' => $query->max_num_pages,
        'format' => '?paged=%#%',
        'current' => $currentPage,
        'prev_text' => __('<i class="fa fa-angle-left"></i>'),
        'next_text' => __('<i class="fa fa-angle-right"></i>'),
        'show_all' => false
    )
);

if ($query->max_num_pages > 1) :
    ?>
<div class="page-nav">
    <div class="pagination-container">

        <ul class="pagination">
            <?php
            foreach ( $paginate as $page ) {

                if(strpos($page,'current')>-1){
                    $liClass = 'active';
                }
                else{
                    $liClass ='';
                }
                echo '<li class="'.$liClass.'">' . $page . '</li>';
            }
            ?>
        </ul>
    </div>
</div>
<?php
endif;

?>
