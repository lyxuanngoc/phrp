<?php
/*
 * Template Name: Article Summary Page
 */

?>

<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<?php

//Which type of articles should we show on this page
$articleType = get_field('article_display_type');

?>

<div class="container">
    <!-- start content container -->


    <?php //left sidebar ?>
    <?php get_sidebar( 'left' ); ?>

    <?php get_template_part('content','header'); ?>

    <?php

    /**
     * If we're looking at the past issue we need to include the CSIRO past issues. Can't be done as a single query since they need to be shown
     * differently
     */


    $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

    $args = array('post_type' => 'article',
        'post_status' => 'publish',
        'posts_per_page' => 10,
        'paged' => $paged,
        'orderby' => 'date',
        'order' => 'DESC',
        'meta_query' => array('relation'=>'AND',array('key'=>'is_issue','value'=>'No','compare'=>'=='),
            array('key'=>'article_type','value'=>$articleType,'compare'=>'==')));

    $tempQry = $wp_query;
    $wp_query = null;

    $wp_query = new WP_Query($args);

    ?>

    <h1><?php the_title(); ?></h1>



    <?php

    /**
     * Shows all articles for the given type defined in ACF fields for this Page Template
     */
    if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();


        $currentArticleType = get_field('article_type');

        IssueArticle::article_snippet(get_post(get_the_ID()),$currentArticleType);

        wp_link_pages(); ?>


    <?php endwhile; ?>

        <?php get_template_part('content','pagination'); ?>

    <?php endif; ?>



    <?php //get the right sidebar ?>
    <?php get_sidebar( 'right' ); ?>


    <!-- end content container -->
</div>
<?php get_template_part('content','footer-subscribe'); ?>
<?php get_template_part('content','footer-topics'); ?>

<?php

//put the query loop back
wp_reset_query();

get_footer(); ?>
