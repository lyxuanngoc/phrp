<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

    <!-- start content container -->
    <div class="container">

    <div class="body_content">
        <?php //left sidebar ?>
        <?php get_sidebar( 'left' ); ?>


        <div class="col-md-<?php devdmbootstrap3_main_content_width(); ?> dmbs-main">
         <h1><?php _e('Sorry This Page Does Not Exist!','devdmbootstrap3'); ?></h1>
            <p>We’re sorry; the page you were looking for seems to have moved. Try starting again from our <a href="/" title="homepage">home page</a>, or use our <a href="/search" title="search">Advanced Search tool</a>.</p>
        </div>

        <?php //get the right sidebar ?>
        <?php get_sidebar( 'right' ); ?>

        </div>
    </div>
    <!-- end content container -->

<?php get_template_part('content','footer-subscribe'); ?>
<?php get_template_part('content','footer-topics'); ?>

<?php get_footer(); ?>
