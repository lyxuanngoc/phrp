<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>


<div class="container">
    <!-- start content container -->


    <?php //left sidebar ?>
    <?php get_sidebar( 'left' ); ?>

    <?php get_template_part('content','header'); ?>


    <?php

    //If this is an issue, load the issue page otherwise load the article page.
    //Could have gone with a page template plugin for this for CPTs but it would have meant an extra field on the screen
    //as well as having all page templates exposed to the articles CPT, confusing the user.
    if(get_field('is_issue') == 'Yes'){
        get_template_part('content-article','issue');
    }
    else {
        get_template_part('content-article', 'article');
    }

    ?>



    <?php //get the right sidebar ?>
    <?php get_sidebar( 'right' ); ?>


    <!-- end content container -->
</div>
<?php get_template_part('content','footer-subscribe'); ?>
<?php get_template_part('content','footer-topics'); ?>

<?php

get_footer(); ?>
