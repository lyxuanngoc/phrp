
<?php

//Search all articles where author acf is any of the authors in this list.


$args = array('post_type' => 'article',
    'post__not_in' => array( $post->ID),
    'meta_query' => array(
        array(
            'key' => 'article_authors_%_article_author',
            'value' => $articleAuthorNames,
            'compare' => 'IN'
        )
    ),
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'orderby' => 'date',
    'order' => 'DESC',
);

$otherArticles = new WP_Query($args);

if($otherArticles->have_posts()) {

    ?>
    <!-- OTHER ARTICLES BY THIS AUTHOR-->
    <div class="panel panel-default">
        <a id="otherArticlesPanel" data-toggle="collapse" data-parent="#otherArticlesPanel" href="#other_articles"
           class="collapsed">
            <div class="panel-heading">
                <h4 class="panel-title heading-collapsed">
                    Other articles by this author
                </h4>
            </div>
        </a>

        <div id="other_articles" class="panel-collapse collapse">
            <div class="panel-body">
                <?php
                while ($otherArticles->have_posts()) : $otherArticles->the_post();

                    //Get the authors that are associated with this other article
                    $otherArticleAuthors = get_field('article_authors',get_the_ID());

                    $otherArticleAuthorNames = array();

                    //To display the authors of the other article just get their names
                    foreach ($otherArticleAuthors as $otherAuthor)
                        $otherArticleAuthorNames[] = $otherAuthor['article_author'];

                 ?>

                    <a href="<?php the_permalink(); ?>" class="other_article_single">
                        <div class="other_article_text">
                            <h5><?php echo the_title(); ?></h5>

                            <p><?php echo implode(', ', $otherArticleAuthorNames); ?></p>
                        </div>
                        <div class="btn btn-blue">
                            <i class="fa fa-angle-right"></i>
                        </div>
                    </a>

                <?php endwhile; ?>

            </div>
        </div>
    </div>

<?php }

