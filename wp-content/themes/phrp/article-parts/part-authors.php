<?php

if(!empty($articleAuthors)){ ?>

<!-- AUTHOR DETAILS -->
<div class="panel panel-default">
    <a id="authorPanel" data-toggle="collapse" data-parent="#authorPanel" href="#author_details" class="collapsed">
        <div class="panel-heading">
            <h4 class="panel-title heading-collapsed">
                Author details
            </h4>
        </div>
    </a>

    <div id="author_details" class="collapse">
        <div class="panel-body">
            <div id="about_the_authors" class="panel_body_section">
                <h4>About the author/s</h4>

                <?php

                //display all authors on this article
                foreach ($articleAuthors as $author) {

                    ?>

                    <p>
                        <strong><?php echo $author['author_name_full']; ?></strong> <?php echo($author['author_affiliations'] ? ' | ' . $author['author_affiliations'] : ''); ?>
                    </p>

                <?php } ?>
            </div>

            <?php
            if ($correspondingAuthor) {

                ?>
                <div id="corresponding_author" class="panel_body_section">
                    <h4>Corresponding author</h4>

                    <p>
                        <strong><?php echo $correspondingAuthor['author_name_full']; ?></strong>

                        <?php if ($correspondingAuthor['author_email_address']) {
                            ?> | <a href="mailto:<?php echo $correspondingAuthor['author_email_address']; ?>"><?php echo $correspondingAuthor['author_email_address']; ?></a>

                        <?php
                        }
                        ?>
                    </p>

                </div>
            <?php } ?>

            <?php $competingInterests = get_field('article_competing_interests');

            if($competingInterests){?>
                <div id="competing_interests" class="panel_body_section">
                    <h4>Competing interests</h4>
                    <p><?php echo $competingInterests; ?></p>
                </div>
            <?php } ?>

            <?php $authorContributions = get_field('author_contributions');

            if($authorContributions){ ?>
                <div id="author_contributions" class="panel_body_section last">
                    <h4>Author contributions</h4>
                    <p><?php echo $authorContributions; ?></p>
                </div>
            <?php } ?>

        </div>
    </div>
</div>

<?php }