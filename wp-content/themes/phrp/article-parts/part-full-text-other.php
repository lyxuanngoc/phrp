<?php $articleFullText = get_field('article_full_text');

if ( function_exists('slb_activate') ) {
	$articleFullText = slb_activate($articleFullText);
}

if($articleFullText && $articleFullText !== ""){ ?>
    <div class="panel panel-default">
        <a id="fullTextPanel" data-toggle="collapse" data-parent="#fullTextPanel" href="#full_text">
            <div class="panel-heading">
                <h4 class="panel-title heading-expanded">
                    Full text
                </h4>
            </div>
        </a>

        <div id="full_text" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-lg-3">
                        <?php $articleTocs = get_field('article_table_of_contents');

                        if($articleTocs){ ?>
                            <div id="table_of_contents" class="full_text_sidebar">
                                <ul class="list-unstyled">
                                    <?php foreach($articleTocs as $tocItem) { ?>
                                        <li><a href="#<?php echo $tocItem['item_link']; ?>"><?php echo $tocItem['item_text']; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>

                        <?php $articleKeyPoints = get_field('article_key_points');
                        if($articleKeyPoints){ ?>

                            <div id="key_points" class="full_text_sidebar">
                                <h4>Key points</h4>
                                <ul class="list-unstyled">
                                    <?php foreach($articleKeyPoints as $keyPoint) { ?>
                                        <li><?php echo $keyPoint['key_point']; ?></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-xs-12 col-lg-9">
                        <?php echo $articleFullText; ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
<?php }