<?php

$articlePdf = get_field('article_pdf');

$articleCitation = get_field('article_citation');

$articleAbstract = get_field('article_abstract');

?>
<form role="form" method="post" id="downloadFileForm" action="<?php echo get_permalink(1478); ?>">

    <input type="hidden" name="article_id" id="article_id" value="<?php echo $post->ID; ?>"/>

<!-- ARTICLE HEADER -->
<div id="article_information">
    <div class="row">
        <div class="col-xs-12 col-lg-8">
            <div class="article_tag_container">
                <ul class="list-unstyled article_tags">

                    <?php

                    //Get all the topics for this article and display them.
                    //When you click them we need to take you to the search results page via a GET call for searching$terms =  wp_get_post_terms($post->ID, 'topics');

                    $terms = get_the_terms($post->ID,'topics');

                    foreach($terms as $term){
                        ?> <li><a href="/<?php echo add_query_arg(array('topic'=>$term->slug),'search-results');?>" title=""><?php echo $term->name; ?></a></li>
                    <?php }
                    ?>
                </ul>

                <a href="javascript:void(0)" class="viewTopics" data-text="View less <i class='fa fa-angle-up'></i>">View
                    more <i class="fa fa-angle-down"></i></a>

            </div>
            <div id="badges">
                <div class="btn <?php echo IssueArticle::$articleTypes[$currentArticleType]['color']; ?>"><?php echo $articleLabel; ?></div>

                <?php if ($articleBadge) { ?>
                    <div
                        class="btn <?php echo IssueArticle::$articleTypes[$currentArticleType]['color']; ?> outline"><?php echo $articleBadge; ?></div>
                <?php } ?>

            </div>
            <h1 class="article_title"><?php echo the_title(); ?></h1>
            <p id="author" class="author"><?php echo implode(', ', $articleAuthorNames); ?></p>

            <?php if ($articleCitation) { ?>
                <p id="citation" class="citation">
                    <?php echo $articleCitation; ?>
                </p>
            <?php } ?>
        </div>
        <div class="col-xs-12 col-lg-4">
            <div class="article_icon_container">
                <ul class="article_icons list-unstyled">
                    <li class="article_icons__list-item">
                        <a href="#" class=" dropdown-toggle" data-toggle="dropdown" id="citation_dropdown">
                            <i class="fa fa-file-text-o"></i>

                            <p>Citation</p>
                        </a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="citation_dropdown">
                            <li role="presentation"><p>Download this citation in one of the formats below:</p></li>

                            <li role="presentation">

                                <button type="submit" value="downloadRis" name="downloadRis" id="downloadRis">

                                        <p>RIS</p>

                                </button>
                                </li>



                            <li role="presentation">

                                <button type="submit" value="downloadCsv" name="downloadCsv" id="downloadCsv">

                                        <p>CSV</p>

                                </button>
                            </li>
                        </ul>


                    </li>

                    <?php if($articlePdf){ ?>
                        <li class="article_icons__list-item">
                            <a href="<?php echo $articlePdf['url']; ?>" title="Download PDF" target="_blank">
                                <i class="fa fa-file-pdf-o"></i>

                                <p>PDF</p>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if($articleAbstract){ ?>
                        <li class="article_icons__list-item">
                            <button class="xml_button" type="submit" value="downloadXml" name="downloadXml" id="downloadXml">

                                    <i class="fa fa-file-code-o"></i>
                                    <span>XML</span>

                            </button>

                        </li>
                    <?php } ?>

                </ul>
            </div>
        </div>
    </div>
</div>


</form>