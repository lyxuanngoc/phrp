<?php

$references = get_field('article_references');

if($references){ ?>

<div class="panel panel-default">
    <a id="referencesPanel" data-toggle="collapse" data-parent="#referencesPanel" href="#references">
        <div class="panel-heading">
            <h4 class="panel-title heading-expanded">
                References
            </h4>
        </div>
    </a>

    <div id="references" class="panel-collapse collapse in">
        <div class="panel-body">
            <ul>
                <?php foreach($references as $key => $value) { ?>

                    <li id="refList<?php echo $key+1; ?>"><?php echo $value['reference']; ?></li>
                <?php } ?>
            </ul>
        </div>
    </div>


</div>

<?php }