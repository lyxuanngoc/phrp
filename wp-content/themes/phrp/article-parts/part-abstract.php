<?php $articleAbstract = get_field('article_abstract');

if($articleAbstract){ ?>

    <div class="panel panel-default">
        <a data-toggle="collapse" id="abstractPanel" data-parent="#abstractPanel" href="#abstract">
            <div class="panel-heading">
                <h4 class="panel-title heading-expanded">
                    Abstract
                </h4>
            </div>
        </a>

        <div id="abstract" class="panel-collapse collapse in">
            <div class="panel-body">
                <?php echo $articleAbstract; ?>
            </div>
        </div>
    </div>

<?php }