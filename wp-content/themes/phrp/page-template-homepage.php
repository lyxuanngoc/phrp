<?php
/*
 * Template Name: Homepage
 */
?>
<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav');

//@@BM specific fields for homepage redesign
$currentIssueTitle = get_field('homepage_current_issue_title', 'option');
$currentIssueSubtitle = get_field('homepage_current_issue_subtitle', 'option');
$currentIssueImageID = get_field('homepage_current_issue_image', 'option');
$currentIssueTitleofImage = get_field('homepage_current_issue_title_of_image', 'option');
$currentIssueTitleofImagePosition = get_field('homepage_current_issue_title_of_image_position', 'option');
$currentIssueImage = wp_get_attachment_image( $currentIssueImageID, 'issue-big');

$inEvidence = get_field('homepage_in_evidence_boxes', 'option');

$news = get_field('homepage_news_and_media_releases', 'option');



?>

<!-- ARTICLE PREVIEWS -->
<div class="article_previews">
<div class="container">
<!--div class="article_preview_top">
    <div class="row">
        <div class="col-xs-12 col-lg-9">
            <h1 id="homepageIssueHeading" data-alternative-text="Most read articles">Current issue highlights</h1>
        </div>
        <div class="col-xs-12 col-lg-3">
            <a id="toggleArticles" data-alternative-text="View current issue highlights" class="btn btn-block btn-green" href="javascript:void(0)">
                View most read articles
            </a>
        </div>
    </div>
</div-->

    <?php

    $tileRows = latestIssueArticles();

    $count = 0;

    ?>

  <section aria-labelledby="homepage-issue-title" class="currentIssueArticles">
    <div class="row row-flex">
      <div class="col-lg-8">
        <div class="homepage-banner">
          <h2 id="homepage-issue-title"><span class="homepage-banner-main"><?php echo $currentIssueTitle; ?></span> <a class="homepage-banner-sideline" href="<?php echo get_permalink(132);?>" ><?php echo $currentIssueSubtitle; ?></a>
          </h2>
          <!--a href="<?php echo get_permalink(132);?>" class="issue-arrow">
            <span class="sr-only">Read the full current issue</span>
            <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
          </a-->
        </div>

        <div id="currentIssueArticles">

          <div class="row row-flex">

          <?php

          //@@BM to check the first three elements -> only them got image
          $index = 1;

          foreach ($tileRows as $row) {

              //get the ID of the article we want to show so we can get fields from it
              $articleId = $row['tile_article_link']->ID;

              //@@BM get image i<div class="row">d
              $articleImageID = get_field('article_image',$articleId);
              $articleImage =  wp_get_attachment_image( $articleImageID, 'issue-hp');

              //get the particular field that has the article type
              $field = get_field_object('article_type',$articleId);

              //get the value of article type
              $articleValue = get_field('article_type',$articleId);

              //get the label for display purposes
              $articleLabel = array_key_exists('choices', $field) ? $field['choices'][ $articleValue ] : '';

              ?>

              <div class="col-xs-12 col-sm-6 col-lg-4">
                  <div class="article_preview article_new <?php echo $articleValue; ?>">
                      <div class="article_preview_body">
                          <h3><a href="<?php echo get_permalink($articleId); ?>"><?php echo $row['tile_text']; ?></a></h3>
                          <p class="article_label"><span class="sr-only">Labelled as </span><?php echo $articleLabel; ?></p>
                      </div>
                      <?php if ($index < 4 && !empty($articleImage)) : ?>
                      <div class="article_preview_header show-1600">
                        <?php echo $articleImage; ?>
                      </div>
                    <?php endif; ?>
                  </div>
              </div>

          <?php

            ++$index;

          }

          ?>

          </div>
        <!-- current issues articles end -->
        </div>

      </div>

      <div class="col-lg-4 hidden-xs hidden-sm hidden-md">
        <div class="current-issue-image">
          <a href="<?php echo get_permalink(132);?>" >
              <?php if(!empty($currentIssueTitleofImage)) : ?>
              <div class="current-issue-image-title <?php echo $currentIssueTitleofImagePosition; ?>"><?php echo $currentIssueTitleofImage; ?></div>
              <?php endif; ?>
              <?php echo $currentIssueImage; ?>
            </a>
        </div>
      </div>

    </div>

  </section>

  <?php

  if (is_array($inEvidence) && count($inEvidence) > 0 ) : ?>

  <section class="inEvidence" aria-labelledby="inEvidenceTitle">
    <h2 class="sr-only" id="inEvidenceTitle">In evidence</h2>

    <div class="row row-flex">

      <?php  foreach ($inEvidence as $row) {

      //echo "<pre>";var_dump($row);echo "</pre>";

      $title = $row["in_evidence_title"];
      $img = wp_get_attachment_image( $row["in_evidence_image"], 'in-evidence');
      $text = $row["in_evidence_text"];
      $link = $row["in_evidence_link"];
      ?>

      <div class="col-lg-4 col-mylg-4">
        <div class="in-evidence-box">
          <h3><?php echo $title; ?></h2>
          <div class="in-evidence-img"><?php echo $img; ?></div>
          <div class="in-evidence-text-box">
          <p class="in-evidence-text"><?php echo $text; ?></p>
          <p class="in-evidence-link">
            <a href="<?php echo $link["url"]; ?>" <?php if ($link["target"] && $link["target"] != "") echo 'target="'.$link["target"].'"'; ?>>
              <?php echo $link["title"]; ?>
              <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
            </a>
          </p>
        </div>
        </div>
      </div>

    <?php

  } ?>

    </div>
  </section>
  <?php endif; ?>

  <div class="row row-flex">

    <div class="col-lg-8 hr">

    <?php
      //Most read articles tab
      //Get the most read articles and display them in descending order.

      $args = array('post_type' => 'article',
          'meta_key' => 'post_views_count',
          'orderby' => 'meta_value_num',
          'order' => 'DESC',
          'posts_per_page' => '6',
          'meta_query' => array('relation'=>'AND',array('key'=>'is_issue','value'=>'No','compare'=>'==')));


      $articles_query = new WP_Query($args);

      echo '<section id="mostReadArticles" aria-labelledby="mostReadTitle"><h2 class="home-section-title" id="mostReadTitle">Most read articles</h2>';
      echo '<ul class="row row-flex">';

      $count = 0;

      //loop through articles
      while($articles_query->have_posts()) :  $articles_query->the_post();

          //get the ID of the article we want to show so we can get fields from it
          $articleId = $articles_query->post->ID;

          //get the particular field that has the article type
          $field = get_field_object('article_type',$articleId);

          //get the value of article type
          $articleValue = get_field('article_type',$articleId);

          //get the label for display purposes
          $articleLabel = $field['choices'][ $articleValue ];

          //get the abstract
          $articleAbstract = get_field('article_abstract',$articleId);

          $title = get_the_title();

          ?>

          <li class="col-xs-12 col-sm-6">
              <div class="article_preview light article_new <?php echo $articleValue; ?>">
                  <div class="article_preview_body">
                      <h3>
                        <a href="<?php the_permalink();?>"><?php echo (strlen($title) > 100 ? strip_tags(substr($title,0,100)).'[…]' : $title); ?></a>
                      </h3>
                      <p class="article_label"><span class="sr-only">Labelled as </span><?php echo $articleLabel; ?></p>
                  </div>
              </div>
          </li>

          <?php

      endwhile;

      //read more article div, this closes the last row.
      echo "</ul></section>";

      wp_reset_query();

    ?>

    </div>

    <div class="col-lg-4">
      <section aria-labelledby="newsTitle">
        <h2 class="home-section-title" id="newsTitle">News and Media Releases</h2>
        <ul class="news-media">
        <?php

          // /echo "<pre>";var_dump($news);echo "</pre>";

          foreach ($news as $new) {

            $id = $new["post"];

            $title = get_the_title($id);
            $permalink = get_the_permalink($id);
            $copy = (strlen(get_the_content("", false, $id)) > 100) ? substr(strip_tags(get_the_content("", false, $id)), 0, 100)."[...]" : get_the_content("", false, $id);

            ?>

            <li>
              <h3><a href="<?php echo $permalink; ?>"><?php echo $title; ?></a></h3>
              <p><?php echo $copy; ?></p>
            </li>

            <?php
          }

        ?>

      </ul>
    </section>
  </div>

    <!-- end container -->
</div>

<!-- end article previews -->
</div>


<?php get_template_part('content','footer-subscribe'); ?>
<?php get_template_part('content','footer-topics'); ?>

<?php get_footer(); ?>
