<?php

/*
Template Name: Search Results Page
*/

//Start the session. This is used for when you go back to the advanced search form so it remembers your choices.
if (!session_id())
    session_start();


/**
 * All the post elements that need to be part of the meta query (ACF fields)
 * Note the article_authors_%_author_name is a special case. When running the WPQuery it will replace
 * the exact match for the meta_key to be LIKE, since we don't know how many authors an article has at run time.
 * This filter is defined in the phrp_issue_article plugin for posts_where.
 * Note that we've added article title as a special case the ignore query elements since it will be searching the "s" field, not an
 * actual ACF field like the others and needs to not be searched, but be referred to in the header further below.
 */

$searchElements = array(
    'metaQueryElements' => array(
        'article_abstract' => array('name' => 'Article Abstract', 'compare' => 'LIKE'),
        'article_doi' => array('name' => 'Article DOI', 'compare' => '=='),
        'article_full_text' => array('name' => 'Article Full Text', 'compare' => 'LIKE'),
        'article_type' => array('name' => 'Article Type', 'compare' => '=='),
        'article_authors_%_author_name' => array('name' => 'Article Authors', 'compare' => '=='),
    ),
    'taxQueryElements' => array(
        'topics' => array('name' => 'Topics', 'field' => 'slug'),
    ),
    'dateQueryElements' => array(
        'after' => array(),
        'before' => array(),
    ),
    'ignoreQueryElements' => array(
        'article_title' => array('name' => 'Article Title'),
    ),
);

//If this is the first visit to the results page, store the post data in the session so we can retrieve it for pagination
if (isset($_POST['sx']) && $_POST['sx'] == 'results') {

    //set initial search state
    unset($_SESSION['search']);

    foreach ($_POST as $key => $value) {
        $_SESSION['search'][$key] = sanitize_text_field($value);
    }

} elseif (isset($_GET['topic'])  && $_GET['topic'] != '') {
    //Search results request from the footer topics links or the article topic links

    //set initial search state
    unset($_SESSION['search']);

    $_SESSION['search']['topics'] = sanitize_text_field($_GET['topic']);
    $_SESSION['search']['sx'] = 'results';

}

//Only run the search process if we're getting a post request
if (isset($_SESSION['search']['sx']) && $_SESSION['search']['sx'] == 'results') {

    $searchData = $_SESSION['search'];

    /**
     * Loop over all the meta query elements and create the meta_query array that Wordpress expects by matching POST variables.
     */
    $metaQuery = array();

    foreach ($searchElements['metaQueryElements'] as $key => $value) {

        if (isset($searchData[$key]) and $searchData[$key] != '') {

            $metaQuery[] = array('key' => $key, 'value' => $searchData[$key], 'compare' => $value['compare']);
        }
    }

    //Add in a clause to only search articles, not issues.
        $metaQuery[] = array('key' => 'is_issue', 'value' => 'No', 'compare' => '==');



    $metaQuery = array_merge(array('relation' => $relation), $metaQuery);

    /**
     * Likewise loop over all the taxonomy elements and map them to the taxonomy search array Wordpress expects
     */
    $taxQuery = array();

    foreach ($searchElements['taxQueryElements'] as $key => $value) {

        if (isset($searchData[$key]) and $searchData[$key] != '') {

            $taxQuery[] = array('taxonomy' => $key, 'field' => $value['field'], 'terms' => $searchData[$key]);
        }

    }

    $taxQuery = array_merge(array('relation' => $relation), $taxQuery);


    /**
     * Likewise loop over all the date query elements and add them to the date query parameter
     */
    $dateQuery = array();
    $dateQueryInner = array();
    foreach ($searchElements['dateQueryElements'] as $key => $value) {

        if (isset($searchData[$key]) and $searchData[$key] != '') {

            $dateQueryInner[$key] = str_replace('/','-',$searchData[$key]);
        }

    }

    $dateQuery[] = $dateQueryInner;

    // get the page the user is requesting via the get param
    $paged = get_query_var('paged') ? get_query_var('paged') : 1;


    //Set the args for the query for meta query (searching ACF fields) as well as tax_query for searching taxonomies.
    //Throw in the s variable so it will search title.

    $args = array('post_type' => 'article',
        'advanced_search_post_title' => $searchData['article_title'],
        'order' => 'DESC',
        'posts_per_page' => '10',
        'meta_query' => $metaQuery,
        'tax_query' => $taxQuery,
        'date_query' => $dateQuery,
        'paged' => $paged,);




}


get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<div class="container">

    <?php //left sidebar ?>
    <?php get_sidebar('left'); ?>

    <?php get_template_part('content', 'header'); ?>

    <?php


    $wp_query = new WP_Query( $args );

    $result_total = $wp_query->found_posts;

    //variables for the 'results x to y of z'
    if($result_total == 0){

        $result_start = $result_end = 0;

    }else{

        $result_start = (($paged - 1) * 10) + 1;
        $result_end = $result_start + count($wp_query->posts) - 1;

    }


    $searchResultHeader = 'Searching content for ';
    $searchResultData = array();
    $searchItemsToNotDisplay = array('sx', 'before', 'after');

    /**
     * Create the "Searching for content.." field mapping from the field IDs to an English sentence.
     * Loop over the requested search data and build the statement from the meta query array. Then add the dates afterward.
     */

    foreach ($searchData as $key => $value) {

        //We've found a searchable element and it's not empty.
        if (!in_array($key, $searchItemsToNotDisplay) && $value != '') {

            //Look up the name in the search query array and add it to our string.
            foreach ($searchElements as $searchKey => $searchValue) {

                if ($searchValue[$key])
                    $searchResultData[] = ('<span class="search-header-text">' . $value . '</span> in <span class="search-header-text">' . $searchValue[$key]['name'] . '</span>');

            }

        }
    }

    //Manually add in the date fields at the end.
    if(isset($searchData['after']) && $searchData['after'] != ''){
        $searchResultData[] = ('published after <span class="search-header-text">' . $searchData['after'] . '</span>');
    }

    if(isset($searchData['before'])  && $searchData['before'] != ''){
        $searchResultData[] = ('published before <span class="search-header-text">' . $searchData['before'] . '</span>');
    }


    //Combine the header and the search results data.
    $searchResultHeader .= implode(', ', $searchResultData);

    ?>

    <h1 class="search-title">

        <?php _e($searchResultHeader); ?>

    </h1>

    <p>

        <?php echo 'showing results ' . $result_start . '-' . $result_end . ' of ' . $result_total; ?>
        <a href="<?php echo get_permalink(391); ?>">(Click here to refine your search)</a>
    </p>
    <?php


    if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();


        $currentArticleType = get_field('article_type');

        IssueArticle::article_snippet(get_post(get_the_ID()),$currentArticleType,'search');


         wp_link_pages();

    endwhile;

        get_template_part('content', 'pagination');

    else :
        // If no content, include the "No posts found" template.
        get_template_part('content', 'none');

    endif;
    ?>

    <?php //get the right sidebar ?>
    <?php get_sidebar('right'); ?>


</div>

<?php get_template_part('content', 'footer-subscribe'); ?>
<?php get_template_part('content', 'footer-topics'); ?>


<?php get_footer(); ?>
