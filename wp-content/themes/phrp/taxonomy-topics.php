<?php
/*
 * Template Name: Topics
 */
?>

<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<?php

// get the page the user is requesting via the get param
$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

// replace the main query loop with ours, but store it first to put it back when we're done.
$tempQry = $wp_query;
$wp_query = null;

//this is making the assumption that the category name is the same as the page slug
$categoryName = $post->post_name;

$loop = new WP_Query( array(  'posts_per_page' => 10, 'category_name' => $categoryName, 'paged' => $paged ) );

$wp_query   = $loop;

?>

<div class="container">
    <!-- start content container -->


    <?php //left sidebar ?>
    <?php get_sidebar( 'left' ); ?>

    <?php get_template_part('content','header'); ?>


    <h1><?php printf( __( '%s', 'phrp' ), single_cat_title( '', false ) ); ?></h1>

    <?php // theloop
    if (  $loop->have_posts() ) : while (  $loop->have_posts() ) : $loop->the_post(); ?>




        <a href="<?php echo the_permalink(); ?>" class="article_snippet_simple">
            <div class="row">
                <div class="col-xs-10">
                    <div class="news_content">
                        <h5><?php echo the_title(); ?></h5>

                        <p class="news_body"><?php


                            global $more;
                            $more = 0;
                            the_content('',true); ?></p>

                        <p class="author">
                            <?php echo the_field('author'); ?>
                        </p>
                    </div>
                </div>
                <div class="col-xs-2">
                    <div class="btn btn-greenDark outline btn-block">
                        <?php  the_time('F j, Y'); ?>
                    </div>
                </div>
            </div>
        </a>

        <?php wp_link_pages(); ?>


    <?php endwhile; ?>

        <?php get_template_part('content','pagination'); ?>

    <?php else: ?>

        <?php get_404_template(); ?>

    <?php endif; ?>



    <?php //get the right sidebar ?>
    <?php get_sidebar( 'right' ); ?>


    <!-- end content container -->
</div>
<?php get_template_part('content','footer-subscribe'); ?>
<?php get_template_part('content','footer-topics'); ?>

<?php

//put the query loop back
$wp_query = NULL;
$wp_query = $tempQry;

get_footer(); ?>
