<?php

/**
 * Outputs tiles ACF for the current page
 */

$address1 = get_field('address_line_1', 'options');
$address2 = get_field('address_line_2', 'options');
$address3 = get_field('address_line_3', 'options');
$phoneNumber = get_field('phone_number', 'options');
$faxNumber = get_field('fax_number', 'options');
$emailAddress = get_field('email_address', 'options');

?>

<div class="contact_form">
    <div class="row">
        <div class="contact-location col-xs-12 col-sm-6 col-md-4">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3312.3835642911486!2d151.19120035124243!3d-33.87977412696596!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12ae2eaddfcd97%3A0x488cb58131a3a170!2s30%20Wentworth%20St%2C%20Glebe%20NSW%202037%2C%20Australia!5e0!3m2!1sen!2s!4v1582862767945!5m2!1sen!2s"
                width="345"
                height="280"
                frameborder="0"
                style="border:0"
            ></iframe>
        </div>
        <div class="contact-details col-xs-12 col-sm-6 col-md-4">
            <h4>Our location</h4>

            <p><?php echo $address1; ?></p>

            <p><?php echo $address2; ?></p>

            <p><?php echo $address3; ?></p>
        </div>
        <div class="contact-details col-xs-12 col-md-4">
            <h4>Contact details</h4>

            <p><?php echo $phoneNumber; ?></p>

            <p><?php echo $faxNumber; ?></p>

            <p><a href="mailto:<?php echo $emailAddress; ?>"><?php echo $emailAddress; ?></a></p>
        </div>
    </div>
</div>