<?php

class EditorialBios
{
    /**
     * @var string
     */
    const FIELD_NAME_ACF = 'on_page_editorial';

    /**
     * @var string
     */
    const DEFAULT_GROUP = 'default';

    /**
     * @var string
     */
    const INTERNATIONAL_GROUP = 'international-panel';

    /**
     * Return ACF bios.
     *
     * @return array
     */
    public function get()
    {
        $rows = get_field(self::FIELD_NAME_ACF);
        $editorialBios = array();

        foreach ($rows as $row) {
            $editorialBios[$this->getBioGroup($row)]['bios'][] = $row;
        }

        return $editorialBios;
    }

    /**
     * Get Group from row bio.
     *
     * @param array $row
     * @return string
     */
    protected function getBioGroup($row)
    {
        return isset($row['group']) ? $row['group'] : self::DEFAULT_GROUP;
    }
}