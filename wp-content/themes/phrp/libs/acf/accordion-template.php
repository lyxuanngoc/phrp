<?php

if (function_exists("register_field_group")) {
    register_field_group(array (
        'id' => 'acf_accordion-template',
        'title' => 'Accordion Template',
        'fields' => array (
            array (
                'key' => 'field_5bd7dd8acf88b',
                'label' => 'Accordions',
                'name' => 'accordions',
                'type' => 'repeater',
                'sub_fields' => array (
                    array (
                        'key' => 'field_5bd7ddabcf88c',
                        'label' => 'Title',
                        'name' => 'title',
                        'type' => 'text',
                        'required' => 1,
                        'column_width' => 20,
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'formatting' => 'html',
                        'maxlength' => '',
                    ),
                    array (
                        'key' => 'field_5bd7ddb6cf88d',
                        'label' => 'Content',
                        'name' => 'content',
                        'type' => 'wysiwyg',
                        'required' => 1,
                        'column_width' => 75,
                        'default_value' => '',
                        'toolbar' => 'full',
                        'media_upload' => 'yes',
                    ),
                    array (
                        'key' => 'field_5bd7f155b4c9c',
                        'label' => 'Expand',
                        'name' => 'expand',
                        'type' => 'true_false',
                        'column_width' => 5,
                        'message' => '',
                        'default_value' => 0,
                    ),
                ),
                'row_min' => '',
                'row_limit' => '',
                'layout' => 'table',
                'button_label' => 'Add Row',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => 'page-template-accordions.php',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}
