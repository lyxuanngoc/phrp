<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<div class="container">
    <!-- start content container -->


    <?php //left sidebar ?>
    <?php get_sidebar( 'left' ); ?>


    <?php // theloop
    if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <?php get_template_part('content','header'); ?>


        <div class="body_content">
            <h1><?php the_title() ;?></h1>

            <?php the_content(); ?>
        </div>

        <?php wp_link_pages(); ?>


    <?php endwhile; ?>
    <?php else: ?>

        <?php get_404_template(); ?>

    <?php endif; ?>



    <?php //get the right sidebar ?>
    <?php get_sidebar( 'right' ); ?>


    <!-- end content container -->
</div>
<?php get_template_part('content','footer-subscribe'); ?>
<?php get_template_part('content','footer-topics'); ?>

<?php get_footer(); ?>
