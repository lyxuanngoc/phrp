<?php

$currentArticleType = get_field('article_type');

$currentArticleTypeData = get_field_object('article_type');

//get the label for display purposes
$articleLabel = $currentArticleTypeData['choices'][$currentArticleType];

//get the article badge for display purposes
$articleBadge = get_field('article_badge');

//Get the authors that are associated with this article

$articleAuthors = get_field('article_authors');

$correspondingAuthor = null;

foreach ($articleAuthors as $author) {

    $authorName = $author['author_name_full'];

        $articleAuthorNames[] = $authorName;

    if($author['corresponding_author'] == 'Yes'){

        $correspondingAuthor = $author;
    }
}



//include the article header
include(locate_template('article-parts/part-header.php')); ?>


    <div class="addthis_sharing_toolbox"></div>

<?php

// Only show the collapse/expand and the authors if you have authors.

if(!empty($articleAuthors) && $currentArticleType != 'news_and_views'){ ?>

    <!-- EXPAND / COLLAPSE BUTTONS -->
    <div class="expand_collapse_btns">
        <div class="row">
            <div class="col-lg-offset-8 col-xs-6 col-lg-2">
                <button id="collapsePanels" class="btn btn-block btn-green">Collapse all</button>
            </div>
            <div class=" col-xs-6 col-lg-2">
                <button id="expandPanels" class="btn btn-block btn-beige">Expand all</button>
            </div>
        </div>
    </div>

<?php } ?>

<?php if ($currentArticleType == 'news_and_views'){
    include(locate_template('article-parts/part-full-text-news.php'));

}else{ ?>

<div class="panel-group" id="accordion">

    <?php

    //Article authors tab

    include(locate_template('article-parts/part-authors.php'));


    //Article abstract tab

    include(locate_template('article-parts/part-abstract.php'));


    //Article full text tab

    include(locate_template('article-parts/part-full-text-other.php'));


    //Article references tab

    include(locate_template('article-parts/part-references.php'));

    //Article other authors tab

    include(locate_template('article-parts/part-other-articles.php'));

?>

</div>

<?php }