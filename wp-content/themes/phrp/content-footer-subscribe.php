<!-- FOOTER TEXT -->
<div class="footer_subscribe">
    <div class="container">
        <div class="row row-flex">
            <div class="col-xs-12 col-lg-4 footer_subscribe__submit-paper">
                <h2>Make an impact</h2>
                <a href="<?php echo get_permalink(20); ?>" class="btn btn-block btn-blueLight">
                    <span class="text">
                        SUBMIT A PAPER
                    </span>
                    <span class="icon">
                        <i class="fa fa-file-text-o"></i>
                    </span>
                </a>
            </div>


            <div class="col-xs-12 col-lg-4">
                <!---form role="subscribe" method="get" id="subscribeForm" action="<?php echo get_permalink(16); ?>">
                    <div class="newsletter_subscribe">
                        <h2>Keep up to date with our latest articles</h2>

                            <div class="newsletter-container">
                                <label for="emailAddress" class="sr-only">Please enter your e-mail address</label>
                                <input class="form-control" name="emailAddress" id="emailAddress" placeholder="Please enter your e-mail address"/>


                                <button class="btn btn-block btn-green">
                                    <span class="text sr-only">Subscribe</span>
                                    <span class="icon"><i class="fa fa-arrow-circle-right"></i></span>
                                </button>

                              </div>
                    </div>
                </form-->
                <h2>Keep up to date with our latest articles</h2>
              <div class="newsletter_subscribe">

                <?php echo do_shortcode('[gravityform id="3" name="Subscribe (only email)" ajax="true" title=false]'); ?>
              </div>
            </div>
            <div class="col-xs-12 col-lg-4">
              <h2>Follow us on Twitter</h2>
              <a class="follow-twitter" href="https://twitter.com/@phrpjournal">
                <span>@phrpjournal</span>
                <span class="twitter-icon"><i class="fa fa-twitter" aria-hidden="true"></i></span>
              </a>
            </div>
        </div>
    </div>
</div>
