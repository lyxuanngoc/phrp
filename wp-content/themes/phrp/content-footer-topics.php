<!-- FOOTER TEXT -->

<?php
$args = array('hide_empty' => false);
$output = 'objects';
$topics = get_terms('topics', $args);

?>
<div class="container">
    <div class="footer_topics">
        <div class="row">
            <div class="col-xs-12">
                <h5>BROWSE BY TOPIC</h5>
            </div>
        </div>
        <div class="row">

            <div class="col-xs-12 col-md-3">
                <ul class="list-unstyled">
                    <?php

                    $count = 0;
                    //display all topics taxonomies
                    foreach ($topics as $topic) {

                        //each topic taxonomoy has an appear in footer ACF field. Only display if that's true.
                        $showInFooter = get_field('appear_in_footer', 'topics_' . $topic->term_id);

                        //If we want to show the topic in the footer, do a get request to the search results page.
                        if ($showInFooter == 'Yes') {?>

                            <li><a href="/<?php echo add_query_arg(array('topic'=>$topic->slug),'search-results')?>"><?php echo $topic->name; ?></a></li>

                        <?php

                        //13 records per column
                        if (++$count % 13 == 0) {
                        ?>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-3">
                    <ul class="list-unstyled">
                        <?php
                        }
                        }
                    }
                    ?>


            </div>
        </div>
    </div>
</div>