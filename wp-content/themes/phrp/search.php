<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<div class="container">

    <?php //left sidebar ?>
    <?php get_sidebar('left'); ?>

    <?php get_template_part('content', 'header'); ?>



    <h1 class="search-title">

        <?php _e('Searching content for ','locale'); ?><span class="search-header-text"><?php the_search_query();?></span>

    </h1>

    <p>

        <?php

        // get the page the user is requesting via the get param
        $paged = get_query_var('paged') ? get_query_var('paged') : 1;

        //variables for the 'results x to y of z'
        $result_total = $wp_query->found_posts;

        //variables for the 'results x to y of z'
        if($result_total == 0){

            $result_start = $result_end = 0;

        }else{

            $result_start = (($paged - 1) * 10) + 1;
            $result_end = $result_start + count($wp_query->posts) - 1;

        }

        echo 'showing results ' . $result_start . '-' . $result_end . ' of ' . $result_total; ?>

        <a href="<?php echo get_permalink(391); ?>">(Click here to refine your search)</a>
    </p>
    <?php


    if (have_posts()) : while(have_posts()) : the_post();

        if(get_field('article_type')){
            $currentArticleType = get_field('article_type');
        }
        else{
            $currentArticleType = 'other';
        }

        IssueArticle::article_snippet(get_post(get_the_ID()),$currentArticleType,'search');

        wp_link_pages();

    endwhile;

        get_template_part('content', 'pagination');

    else :
        // If no content, include the "No posts found" template.
        get_template_part('content', 'none');

    endif;
    ?>

    <?php //get the right sidebar ?>
    <?php get_sidebar('right'); ?>


</div>

<?php get_template_part('content', 'footer-subscribe'); ?>
<?php get_template_part('content', 'footer-topics'); ?>


<?php get_footer(); ?>
