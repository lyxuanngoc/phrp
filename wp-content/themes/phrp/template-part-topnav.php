
<?php if ( has_nav_menu( 'main_menu' ) ) : ?>

        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="navbar-collapse collapse" id="bs-example-navbar-collapse-2" aria-expanded="false">
                    <?php
                    wp_nav_menu( array(
                            'theme_location'    => 'main_menu',
                            'depth'             => 2,
                            'container'         => 'ul',
                            'container_class'   => 'collapse navbar-collapse navbar-1-collapse',
                            'menu_class'        => 'nav navbar-nav',
                            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                            'walker'            => new wp_bootstrap_navwalker())
                    );
                    ?>
                    <form class="navbar-form navbar-right" role="search" action="<?php echo home_url(); ?>">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search" name="s" id="s" >
                        </div>
                        <button type="submit" class="btn btn-green">
                            <i class="fa fa-search"></i>
                        </button>
                    </form>
                </div>
            </div>
        </nav>

<?php endif; ?>