<?php

/**
 * Outputs tiles ACF for the current page
 */

$rows = get_field('on_page_tile');

if ($rows) {

    $count = 0;

    //initial div
    echo '<div class="row">';

    foreach ($rows as $row) {

        ?>


            <div class="col-xs-12 col-lg-4">
                <a href="<?php echo $row['tile_link']; ?>" class="tile">
                    <div class="tile_header">
                        <h3><?php echo $row['tile_header']; ?></h3>
                        <span><i class="fa fa-angle-right"></i></span>
                    </div>
                    <div class="tile_body">
                        <p><?php echo $row['tile_body_text']; ?></p>
                    </div>
                </a>
            </div>


        <?php
        //echo new row when it's finishing outputting the THIRD item.
        if (++$count % 3 == 0) {
            echo '</div><div class="row">';
        }

    }

    //final div
    echo "</div>";

}

?>
