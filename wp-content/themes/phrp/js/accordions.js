(function($) {
  /**
   * Listens for panel collapses or expands with the class "accordion-panel"
   */
  $('.accordion-panel').on('show.bs.collapse hide.bs.collapse', function () {
    $(this).find('h4').toggleClass("heading-collapsed heading-expanded");
  });
})(jQuery);