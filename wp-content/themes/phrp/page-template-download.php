<?php
/*
 * Template Name: Download File Template
 */

/**
 * @param $filename
 * @param $data
 *
 * Helper function used to export CSV data
 */
function exportData($filename,$data,$contentType){
    ob_clean();
    ob_start();

    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header('Content-Description: File Transfer');
    header($contentType);
    header("Content-type: application/force-download");
    header("Content-Disposition: attachment; filename={$filename}");
    header("Expires: 0");
    header("Pragma: public");

    $fh = @fopen( 'php://output', 'w' );

    foreach($data as $row){
        echo $row . PHP_EOL;
    }

    fclose($fh);
    exit;

}

/**
 * @param $data
 * @return string
 *
 * Simple helper function to convert an array to XML for outputting.
 */
function arrayToXml($data){

    /* create a dom document with encoding utf8 */
    $domtree = new DOMDocument('1.0', 'UTF-8');

    /* create the root element of the xml tree */
    $xmlRoot = $domtree->createElement("article");
    /* append it to the document created */
    $xmlRoot = $domtree->appendChild($xmlRoot);

    foreach($data as $key => $value){
        if(is_array($value)){

            //create the authors element inside article
            $xmlAuthor = $domtree->createElement("authors");
            $xmlAuthor = $xmlRoot->appendChild($xmlAuthor);

            foreach($value as $innerKey => $innerValue){
                $xmlRow = $domtree->createElement('author',$innerValue);
                $xmlRow = $xmlAuthor->AppendChild($xmlRow);
            }
        }else {
            $xmlRow = $domtree->createElement($key, $value);
            $xmlRow = $xmlRoot->AppendChild($xmlRow);
        }
    }

    /* get the xml printed */
    return $domtree->saveXML();

}

/**
 * @param $key
 * @param $value
 * @return string
 *
 * Simply a helper function to create the RIS format key value pair for output
 */
function keyValueForRis($key,$value){
    return $key . '  - ' . $value;
}

/**
 * @param $articleId
 *
 * Gets RIS data formatted array for the current article ID
 */
function getRisData($articleId){

    $article_type = get_field('article_type', $articleId);

    //set the header title based on the article type.
    $headerTitle = ($article_type == 'nsw_public_health_bulletin_archive' ? 'NSW Public Health Bulletin' : 'Public Health Research & Practice');

    $risArray = array();

    //Load default header
    $risArray[] = keyValueForRis('TY','JOUR');

    //Load all of the article authors in the required AU format.
    while(the_repeater_field('article_authors',$articleId)){

        $nameExportFormat = implode(', ',array(get_sub_field('author_surname'),get_sub_field('author_first_name')));

        $risArray[] = keyValueForRis('AU',$nameExportFormat);

    }

    //Load all of the custom citation data for this article
    while(the_repeater_field('article_citation_data',$articleId)){

        $risArray[] = keyValueForRis(get_sub_field('citation_key'), get_sub_field('citation_value'));

    }

    if(get_sub_field('article_abstract')){
        $risArray[] = keyValueForRis('AB', get_sub_field('article_abstract'));
    }

    if(get_sub_field('article_doi')){
        $risArray[] = keyValueForRis('DO', fix_doi_domain(get_sub_field('article_doi')));
    }

    $risArray[] = keyValueForRis('T1',get_the_title($articleId));

    $risArray[] = keyValueForRis('T2',$headerTitle);

    $risArray[] = keyValueForRis('UR',$_SERVER['HTTP_REFERER']);
    
    //Apparently every RIS file needs this at the end.
    $risArray[] = keyValueForRis('ER','');

    $article = get_post($articleId);
	$risArray[] = keyValueForRis('Volume', get_field('volume', $article->post_parent));
	$risArray[] = keyValueForRis('Issue', get_field('issue', $article->post_parent));
	$risArray[] = keyValueForRis('DOI', fix_doi_domain(get_field('article_doi', $articleId)));
	$risArray[] = keyValueForRis('Year', date('Y', strtotime($article->post_date)));

    return $risArray;

}

/**
 * @param $articleId
 * @return array
 *
 * Simply returns citation data as a CSV
 */
function getCsvData($articleId){

    $article_type = get_field('article_type', $articleId);

    //set the header title based on the article type.
    $headerTitle = ($article_type == 'nsw_public_health_bulletin_archive' ? 'NSW Public Health Bulletin' : 'Public Health Research & Practice');

    $data = array();
    $header = array();

    //Load default header
    $header[] = 'Data Type';
    $data[] = 'Journal';

    //Load all of the article authors in the required AU format.
    while(the_repeater_field('article_authors',$articleId)){

        $nameExportFormat = implode(', ',array(get_sub_field('author_surname'),get_sub_field('author_first_name')));

        $data[] = $nameExportFormat;
        $header[] = 'Author';

    }

    //Load all of the custom citation data for this article
    while(the_repeater_field('article_citation_data',$articleId)){

        $subField = get_sub_field_object('citation_key');
        $subFieldChoice = get_sub_field('citation_key');

        $data[] = get_sub_field('citation_value');
        $header[] = $subField['choices'][$subFieldChoice];

    }

    if(get_sub_field('article_abstract')){
        $data[] = get_sub_field('article_abstract');
        $header[] = 'Abstract';
    }

    if(get_field('article_doi')){
        $data[] = fix_doi_domain(get_field('article_doi'));
        $header[] = 'DOI';
    }

	$article = get_post($articleId);

    $data[] = get_the_title($articleId);
    $data[] = $headerTitle;
    $data[] = $_SERVER['HTTP_REFERER'];
	$data[] = get_field('volume', $article->post_parent);
	$data[] = get_field('issue', $article->post_parent);
	$data[] = date('Y', strtotime($article->post_date));

    $header[] = 'Title';
    $header[] = 'Source';
    $header[] = 'Source URL';
    $header[] = 'Volume';
    $header[] = 'Issue';
    $header[] = 'Year';


    $returnData = array();
    $returnData[] = implode(',',$header);
    $returnData[] = '"'. implode('","',$data) . '"';


    return $returnData ;

}

//Post submission via one of the buttons.
if(!empty($_POST)) {

    $articleId = $_POST['article_id'];

    //get the slug for the file name
    $post = get_post($articleId);

    $slug = $post->post_name;

    if(isset($_POST['downloadRis']) && $_POST['downloadRis'] == 'downloadRis'){

        $risData = getRisData($articleId);

        exportData($slug . '-citation.ris',$risData,'Content-type: text/csv');

    }

    if(isset($_POST['downloadCsv']) && $_POST['downloadCsv'] == 'downloadCsv'){

        $csvData = getCsvData($articleId);

        exportData($slug . '-citation.csv',$csvData,'Content-type: text/csv');

    }

    if(isset($_POST['downloadXml']) && $_POST['downloadXml'] == 'downloadXml'){


        $data = array();

        $authorArray = array();

        $article_type = get_field('article_type', $articleId);

        //set the header title based on the article type.
        $headerTitle = ($article_type == 'nsw_public_health_bulletin_archive' ? 'NSW Public Health Bulletin' : 'Public Health Research & Practice');


        //get the authors
        while(the_repeater_field('article_authors',$articleId)){

            $nameExportFormat = implode(', ',array(get_sub_field('author_first_name'),get_sub_field('author_surname')));
            $authorArray[] = $nameExportFormat;

        }

        $data[] = arrayToXml(array(
                                    'title' => $post->post_title,
                                    'journal_name' => $headerTitle,
                                    'volume' => get_field('volume',$post->post_parent),
                                    'issue' => get_field('issue',$post->post_parent),
                                    'year' => date('Y', strtotime($post->post_date)),
                                    'doi' => fix_doi_domain(get_field('article_doi',$articleId)),
                                    'abstract' => get_field('article_abstract',$articleId),
                                    'authors' => $authorArray,
        ));
        exportData($slug . '-abstract.xml',$data,'Content-type: text/xml');

    }

}

function fix_doi_domain($doi) {
    return (strpos($doi, 'dx.doi.org')) ? ltrim($doi, 'doi:') : 'http://dx.doi.org/' . $doi;
}

//If you're not posting then you need to be 404'd, naughty, no direct access!
global $wp_query;
$wp_query->set_404();
status_header( 404 );
get_template_part( 404 ); exit();
