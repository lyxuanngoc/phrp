<?php
/*
 * Template Name: Issue Summary Page
 */

/**
 * Appears before the first set of tiles
 */

function preTileWrapper(){ ?>
    <div class="row">
    <div class="article_previews">
    <div class="container">
    <div class="article_row">
    <div class="row">
<?php }

/**
 * Appears after either the end of the row of tiles, or the last row
 */
function postTileWrapper(){?>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="row">
    <div class="article_previews">
    <div class="container">
    <div class="article_row">
    <div class="row">
<?php }

/**
 * Closing divs for tile rows
 */
function closingRow(){ ?>
    </div>
    </div>
    </div>
    </div>
    </div>
<?php }

/**
 * @param $type
 * Displays a tile. The type determines the coloring.
 */
function displayTile($type){?>

    <div class="article-tile col-xs-6 col-sm-4 col-lg-2">
        <div class="article_preview <?php echo $type; ?>">
            <?php $featured_article = get_field('featured_article');?>
            <div class="article_preview_header">
                <p><?php echo get_field('issue_title_text'); ?></p>
            </div>
            <div class="article_preview_under_title">
                <div class="article_preview_body">
                    <p><strong><?php echo get_field('issue_inner_text'); ?></strong></p>
                </div>
                <?php if($featured_article) : ?>
                <div class="article_preview_featured">
                    <p><strong><?php echo $featured_article->post_title;?></strong></p>
                </div>
                <?php endif; ?>
            </div>
            <div></div>
            <div class="article_preview_footer">
                <a href="<?php echo the_permalink(); ?>" class="read_more">READ MORE <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
<?php
}
?>

<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<?php

//simple name mapping array for the issue key to what we want to show on this page
$issueNameMap = array('Past Issue' => 'Past issues',
                        'Past Issue CSIRO' => 'Past issues (NSW Public Health Bulletin)',
                        'Supplement' => 'Supplements',
                        'Supplement CSIRO' => 'Supplement issues (NSW Public Health Bulletin',);

//Which type of issue should we show on this page
$issueType = get_field('issue_display_type');

/**
 * If we're looking at the past issue we need to include the CSIRO past issues. Can't be done as a single query since they need to be shown
 * differently
 */

$issueLookupType = null;

if($issueType == 'Past Issue'){

    $issueLookupType = 'Past Issue CSIRO';
    $pastIssueHeader = 'Past Issues (NSW Public Health Bulletin)';

}elseif($issueType == 'Supplement'){

    $issueLookupType = 'Supplement CSIRO';
    $pastIssueHeader = 'Past Supplements (NSW Public Health Bulletin)';


}

if($issueLookupType){
    $extraIssueType = array($issueLookupType);

    $args = array('post_type' => 'article',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'DESC',
        'meta_query' => array('relation'=>'AND',array('key'=>'is_issue','value'=>'Yes','compare'=>'=='),
            array('key'=>'issue_type','value'=>$extraIssueType,'compare'=>'IN')));

    $extra_issues_query = new WP_Query($args);
}

$args = array('post_type' => 'article',
    'posts_per_page' => -1,
    'orderby' => 'date',
    'order' => 'DESC',
    'meta_query' => array('relation'=>'AND',array('key'=>'is_issue','value'=>'Yes','compare'=>'=='),
                                                array('key'=>'issue_type','value'=>$issueType,'compare'=>'==')));

$issues_query = new WP_Query($args);

?>

<div class="container">
    <!-- start content container -->


    <?php //left sidebar ?>
    <?php get_sidebar( 'left' ); ?>

    <?php get_template_part('content','header'); ?>


    <h1><?php echo $issueNameMap[$issueType]; ?></h1>

        <p><?php the_content();?></p>

<div class="past_issues">

    <?php

    preTileWrapper();

    $count = 0;

    /**
     * Shows all issues in a given section ie Past Issues or Supplements
     */
    if ( $issues_query->have_posts() ) : while ( $issues_query->have_posts() ) : $issues_query->the_post();

            
        displayTile('research');

        //echo new row when it's finishing outputting the THIRD item.
        if (++$count % 6 == 0) {
            postTileWrapper();
        }

        ?>

    <?php endwhile; ?>

    <?php endif; ?>



    <!-- closing divs for the first past issues series of rows -->
    <?php closingRow(); ?>

    <?php

    /**
     * Shows all the issues in the Past Issues CSIRO section if there are any
     */
    if($extra_issues_query){
        if ( $extra_issues_query->have_posts() ) { ?>

            <h2><?php echo $pastIssueHeader; ?></h2>
            <hr>
            <p>In June 2014 the <i>NSW Public Health Bulletin</i> changed its name to <i>Public Health Research &amp; Practice</i> and the Sax Institute became the journal's publisher.</p>

            <div id="pastIssueCsiro">
            <?php

            preTileWrapper();

            $count = 0;

            while ( $extra_issues_query->have_posts() ) : $extra_issues_query->the_post();



                displayTile('perspectives');

                //echo new row when it's finishing outputting the THIRD item.
                if (++$count % 6 == 0) {
                    postTileWrapper();
                }

                ?>

      <?php endwhile; ?>



    <?php } } ?>

    <!-- closing divs for the first past issues series of rows -->
    <?php closingRow(); ?>

            <!-- past issue csiro id container -->
        </div>

    <?php if($extra_issues_query) { ?>

        <a href="javascript:void(0)" class="show_more">
            <p>SHOW MORE</p>
        </a>
   <?php } ?>

    <!-- past issues container -->
</div>

    <?php //get the right sidebar ?>
    <?php get_sidebar( 'right' ); ?>


    <!-- end content container -->
</div>
<?php get_template_part('content','footer-subscribe'); ?>
<?php get_template_part('content','footer-topics'); ?>

<?php

//put the query loop back
wp_reset_query();

get_footer(); ?>
