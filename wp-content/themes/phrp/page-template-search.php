<?php

/*
Template Name: Search Page
*/

if (!session_id())
    session_start();

get_header();


//topics for the search page
$args = array('hide_empty' => false);
$output = 'objects';
$topics = get_terms('topics', $args);

//If the user has search data stored in their session from a previous search, restore it.
if(isset($_SESSION['search'])){
    $searchOptions = $_SESSION['search'];
}

?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>


<div class="container">
    <!-- start content container -->


    <?php //left sidebar ?>
    <?php get_sidebar( 'left' ); ?>
    <?php get_template_part('content','header'); ?>

    <h1><?php the_title() ;?></h1>
    <?php echo the_content();?>
    <div class="advanced_search">
        <form class="advanced_search_form" role="search" method="post" id="searchform" action="<?php echo get_permalink(395); ?>">
           <input type="hidden" name="sx" id="sx" value="results"/>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label>Date:</label>
                        <i class="fa fa-question-circle phrp_tooltip" data-toggle="tooltip" data-placement="left"
                           title="Select date range for published articles"></i>
                        <div class="row published_date">
                            <div class="col-xs-3">
                                <p>Published between</p>
                            </div>
                            <div class="col-xs-4">
                                <input class="form-control datePicker" id="after" name="after" autocomplete="off" data-date-format="yyyy-mm-dd" value="<?php echo $searchOptions['after']; ?>"/>
                            </div>
                            <div class="col-xs-1">
                                <p>and</p>
                            </div>
                            <div class="col-xs-4">
                                <input class="form-control datePicker" id="before" name="before" autocomplete="off" data-date-format="yyyy-mm-dd" value="<?php echo $searchOptions['before']; ?>"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label>doi:</label>
                        <input class="form-control" id="article_doi" name="article_doi" value="<?php echo $searchOptions['article_doi']; ?>"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label>Article Type (applies to articles published after October 2014):</label>
                        <i class="fa fa-question-circle phrp_tooltip"  data-toggle="tooltip" data-placement="left"
                           title="This categorisation system was introduced in October 2014. Please don't select this option if you wish to search the entire archive."></i>

                        <select class="form-control" id="article_type" name="article_type">
                            <option value="">Please select an article type</option>
                            <?php

                            foreach(IssueArticle::$articleTypes as $key => $value){
                                if($value['searchable']){ ?>
                                    <option <?php echo ($searchOptions['article_type'] == $key ? 'selected="selected"':''); ?> value="<?php echo $key; ?>"><?php echo $value['name']; ?></option> <?php
                                }
                            }
                            ?>

                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label>Article topic:</label>

                        <select class="form-control" id="topics" name="topics">
                            <option value="">Please select an article topic</option>
                                <?php

                                foreach($topics as $topic){

                                    //if you've selected a topic previously from another search then preload it.
                                    $selected = ($topic->slug == $searchOptions['topics'] ? 'selected="selected"':'');

                                    echo '<option ' . $selected . 'value="' . $topic->slug . '">' . $topic->name . '</option>';
                                }

                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label>Author:</label>
                        <i class="fa fa-question-circle phrp_tooltip"  data-toggle="tooltip" data-placement="left"
                           title="Search for a particular author"></i>
                        <input class="form-control" id="article_authors_%_author_name" name="article_authors_%_author_name" value="<?php echo $searchOptions['article_authors_%_author_name']; ?>"/>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label>Abstract:</label>
                        <i class="fa fa-question-circle phrp_tooltip" data-toggle="tooltip" data-placement="left"
                           title="Search for text in abstract only"></i>
                        <input class="form-control" id="article_abstract" name="article_abstract" value="<?php echo $searchOptions['article_abstract']; ?>"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label>Title:</label>
                        <i class="fa fa-question-circle phrp_tooltip" data-toggle="tooltip" data-placement="left"
                           title="Search for text in title only"></i>
                        <input class="form-control" id="article_title" name="article_title" value="<?php echo $searchOptions['article_title']; ?>"/>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label>Full text:</label>
                        <i class="fa fa-question-circle phrp_tooltip" data-toggle="tooltip" data-placement="left"
                           title="Search within the full text of the article only. Note: full text search is not applicable for all older articles, which may be available in PDF format only."></i>
                        <input class="form-control" id="article_full_text" name="article_full_text" value="<?php echo $searchOptions['article_full_text']; ?>"/>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <button type="submit" id="searchFormButton" class="btn btn-blue btn-block large">
                    SEARCH
                </button>
            </div>
        </div>
    </div>


    <?php //get the right sidebar ?>
    <?php get_sidebar( 'right' ); ?>


    <!-- end content container -->
</div>


<?php get_template_part('content','footer-subscribe'); ?>
<?php get_template_part('content','footer-topics'); ?>

<?php get_footer(); ?>