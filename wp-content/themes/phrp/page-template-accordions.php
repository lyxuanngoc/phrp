<?php
/*
 * Template Name: Accordions Template
 */

$accordions = get_field('accordions') ? get_field('accordions') : array();
?>

<?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<div class="container">
    <?php get_template_part('content', 'header'); ?>

    <h1><?php the_title();?></h1>

    <?php the_content();?>

    <div class="panel-group" id="accordions">
        <?php
        $key = 0;
        foreach ($accordions as $accordion) {
            $key++;
            ?>
            <div class="accordion-panel panel panel-default">
                <a id="<?php echo 'panel-' . $key; ?>"
                   data-toggle="collapse"
                   data-parent="<?php echo '#panel-' . $key; ?>"
                   href="<?php echo '#' . $key; ?>"
                >
                    <div class="panel-heading">
                        <h4 class="panel-title <?php echo ($accordion['expand'] ? ' heading-expanded' : ' heading-collapsed'); ?>"><?php echo $accordion['title']; ?></h4>
                    </div>
                </a>
                <div id="<?php echo $key; ?>" class="panel-collapse collapse <?php echo ($accordion['expand'] ? ' in' : ''); ?>">
                    <div class="panel-body"><?php echo $accordion['content']; ?></div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<?php get_template_part('content','footer-subscribe'); ?>
<?php get_template_part('content','footer-topics'); ?>

<?php get_footer(); ?>
