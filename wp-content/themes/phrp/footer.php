
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    wp_nav_menu( array(
                            'theme_location'    => 'footer_menu',
                            'depth'             => 1,
                            'container'         => 'ul',
                            'container_class'   => 'collapse navbar-collapse navbar-2-collapse',
                            'menu_class'        => 'list-unstyled footer__text',
                            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                            'walker'            => new wp_bootstrap_navwalker())
                    );
                    ?>
                </div>

                <div class="col-xs-12">
                    <div class="footer__bottom">
                        <div class="footer__copyright-text">
                            <p >
                                Copyright <a href="http://www.saxinstitute.org.au/" target="_blank">Sax Institute</a> 2014 <br class="hidden-xs hidden-sm hidden-md" />
                            </p>
                        </div>

                        <div class="footer__buttons">
                            <div class="twitter-follow">
                                <a href="https://twitter.com/phrpjournal" class="twitter-follow-button" data-show-count="false" data-size="large" data-show-screen-name="false">Follow @phrpjournal</a>
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                            </div>

                            <a class="creative-commons" target="_blank" rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/" data-toggle="tooltip" data-placement="top" title="This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License">
                                <img alt="Creative Commons License" src="<?php echo get_stylesheet_directory_uri() ?>/img/creativeCommons.png" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<!-- end main container -->

<?php wp_footer(); ?>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-53067314-1', 'auto');
        ga('send', 'pageview');

    </script>
    
</body>
</html>