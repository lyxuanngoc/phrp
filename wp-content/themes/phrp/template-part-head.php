<?php global $dm_settings; ?>

<?php

//Add page counter for the current page/post if the ID is available.
//Originally just for the most read on the homepage however start tracking everything for future use if required.
$id = get_the_ID();
if($id){
    do_shortcode('[update_page_counter post_id="' . $id . '"]');
}

?>
<div class="dmbs-container">

<?php if ($dm_settings['show_header'] != 0) : ?>

    <div class="container">
        <div class="header">
            <div class="container-fluid">
                <div class="header__branding">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo">
                        <?php if (!is_front_page()) : ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo-v2.png" />
                        <?php else : ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/PHRP_LOGO_LEADINGJOURNAL_POS_RGB.svg" width="419"/>
                        <?php endif; ?>
                    </a>
                </div>
                <div class="header__right">
                    <a class="hidden-xs" href="https://www.saxinstitute.org.au/" target="_blank" title="Visit Sax Institute">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo_sax-journal-v2.png" title="Visit Sax Institute" alt="A journal of the Sax Institute"/>
                    </a>
                    <a href="/search" class="advanced_search">Advanced search <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>
