<?php

/*
Plugin Name: PHRP Existing Issues Migrator
Plugin URI: http://4mation.com.au
Description: Declares a plugin to import existing issues
Version: 1.0
Author: 4mation
Author URI: http://4mation.com.au
*/


class ExistingIssuesMigrator extends PluginManagerRegisterBase
{

    public static function get_actions()
    {

        return array(
            'admin_menu' => 'register_menu',
        );

    }

    function register_menu()
    {
        add_management_page( 'Existing Issues Migrator', 'Existing Issues Migrator', 'manage_options', basename(__FILE__), array( $this, 'existing_issues_migrator' ));
    }


    /**
     * @param $acfname
     * @return string $acfkey
     *
     * This function takes in a ACF field name (eg: is_issue) and returns the corresponding ACF field key.
     * It is needed by the ACF update_field() function
     * (Reference: http://www.advancedcustomfields.com/resources/update_field/)
     */
    function get_acfkey($acfname)
    {
        $acf_name_to_key_mapping = array(
            'is_issue' => 'field_53d1c36fc5b11',
            'issue_type' => 'field_53d1c35ea1579',
            'featured_article' => 'field_53f2b4e1996e2',
            'issue_title_text' => 'field_53d9b54a78b48',
            'issue_inner_text' => 'field_53d9b57078b49',
            'article_type' => 'field_53d1f172393bd',
            'article_doi' => 'field_53f29a8a57ea7',
            'article_pdf' => 'field_53f189ae57aa3',
            'article_abstract' => 'field_53eab30e815a9',
            'article_badge' => 'field_53d5e903da7d0',
            'article_corresponding_author' => 'field_53e98d888e9b1',
            'article_competing_interests' => 'field_53e9b140e21b9',
            'author_contributions' => 'field_53e9b152e21ba',
            'article_references' => 'field_53eabb176ecda',
            'article_full_text' => 'field_53eae60c33728',
            'article_table_of_contents' => 'field_53eaebb4b54dc',
            'article_key_points' => 'field_53eaeb7353792',
            'article_authors' => 'field_53fbe9c78cf18'
        );
        return $acf_name_to_key_mapping[$acfname];
    }


    /**
     * @param $field_name
     * @param $value
     * @param $post_id
     *
     * This is a wrapper function for ACF's update_field function, which:
     * 1. Trims the string
     * 2. Checks for null value
     * Before adding it to the ACF field
     */
    function update_acf_field($field_name, $value, $post_id) {
        $field_key = $this->get_acfkey($field_name);
        $value = trim($value);
        if ($value) {
            // Only update field if the value is not empty
            update_field( $field_key, $value, $post_id );
        }
    }

    /**
     * @param $folder
     * @return int
     *
     * This function will create a new 'issue' post.
     * Input: The folder of the issue
     * Returns the issue's post_id upon success, or the error message if unsuccessful
     */
    function add_issue( $folder ) {

        $field_mapping = array(
            'issue_title_text' => '{{JOUR_YEAR}}',
            'issue_inner_text' => 'VOLUME {{VOLUME}} ISSUE {{ISSUE}}' ,
        );

        $parsed_info = array_fill_keys(
            array('JOURNAL', 'VOLUME', 'JOURNAL URL', 'ISSUE', 'ISSN', 'JOUR_YEAR', 'PUB DATE'), ''
        );

        $manifest_file = $folder  . "/cpec_manifest.txt";
        if (!file_exists ($manifest_file)) {
            return "cpec_manifest.txt not found in: ". $folder . "\n";
        }
        $manifest_handle = fopen($manifest_file, 'r');
        if (!$manifest_handle) {
            return "Error reading file: ". $manifest_file . "\n";
        }

        // Parse fields and their values row by row until we reach the "--- Files ---" row
        while (($line = fgets($manifest_handle)) !== false) {
            if ($line == "--- Files ---")
                break;
            if (strpos($line, ': ' )) {
                $row_data = explode(": ", $line);
                if (array_key_exists($row_data[0], $parsed_info)) {
                    $parsed_info[$row_data[0]] = str_replace("\n", '', $row_data[1]);
                }
            }
        }

        //Format pub date for front end formatting. If we don't have journal year use the pub date,
        //and put that back in the array for the inner text.
        if ($parsed_info['JOUR_YEAR'] != '') {

            $date = DateTime::createFromFormat('d/m/Y', $parsed_info['JOUR_YEAR']);

        } else {

            $date = DateTime::createFromFormat('d/m/Y', $parsed_info['PUB DATE']);

        }

        $parsed_info['JOUR_YEAR'] = $date->format('F Y');

        // Create the 'issue' post and set it's custom fields
        $post_id = wp_insert_post(array (
            'post_type' => 'article',
            'post_title' => ('Volume ' . $parsed_info['VOLUME'] . ' Issue ' . $parsed_info['ISSUE']),
            'post_status' => 'publish',
            'post_date' => $date->format('Y-m-d'),
        ));



        if ($post_id) {
            // update ACF field values
            $this->update_acf_field( 'is_issue', "Yes", $post_id );
            $this->update_acf_field( 'issue_type', "Past Issue CSIRO", $post_id);

            foreach ($field_mapping as $field => $field_val) {
                // Replace placeholder variables in string
                foreach ($parsed_info as $key => $value) {
                    $field_val = str_replace('{{'.$key.'}}', $value, $field_val);
                }
                if ($field_val != ''){
                    $this->update_acf_field( $field, $field_val, $post_id);
                }
            }
        }

        fclose($manifest_handle);
        return $post_id;
    }


    /**
     * @param $folder
     * @param $issue_post_id
     * @return int
     *
     * This function will parse the issue manifest, and create article posts for each of the article listed in the manifest
     * Input: The folder of the issue
     * Returns an array with an element for each article, containing:
     * - The article post_id if it was created successfully
     * - The error message otherwise
     */
    function add_articles ( $folder, $issue_post_id ) {
        // Get article list
        $manifest_file = $folder  . "/cpec_manifest.txt";

        // Parse manifest
        if (!file_exists ($manifest_file)) {
            return "cpec_manifest.txt not found in: ". $folder . "\n";
        }
        $manifest_handle = fopen($manifest_file, 'r');
        if (!$manifest_handle) {
            return "Error reading file: ". $manifest_file . "\n";
        }

        // Fetch article names from manifest files, and create the 'article' posts
        $begin_reading_files = false;
        while (($line = fgets($manifest_handle)) != false) {
            if ( strpos($line, "--- Files ---") !== false ) {
                $begin_reading_files = true;
                break;
            }
        }
        $rtn_vals = array();
        if ($begin_reading_files) {
            while (($line = fgets($manifest_handle)) !== false && $line != '') {
                if ( strpos($line, "abs.xml") != false){
                    $filename = str_replace(array("\r\n", "\r", "\n"),'', $line);
                    $pdf_filename = str_replace("abs.xml", ".pdf", $filename);
                    if (!file_exists ($folder  . "/" . $pdf_filename)) {
                        $pdf_filename = null;
                    }
                    $rtn_val = $this->add_article ( $filename, $folder, $issue_post_id, $pdf_filename );
                    array_push($rtn_vals, $rtn_val);
                }
            }
        }
        fclose($manifest_handle);
        return $rtn_vals;
    }


    /**
     * @param $filename
     * @param $folder
     * @param $issue_post_id
     * @param null $pdf_filename
     * @return int
     *
     * This function will create a new 'article' post.
     * Input: The folder of the issue
     * Returns the article's post_id upon success, or the error message if unsuccessful
     */
    function add_article( $filename, $folder, $issue_post_id, $pdf_filename = null ) {
        $article_manifest = $folder  . "/" . $filename;

        if (!file_exists ($article_manifest)) {
            return $filename. " not found in: ". $folder . "\n";
        }
        $parsed_xml=simplexml_load_file($article_manifest);


        //Get the post date of the parent issue and mark it as the same for the article.

        $postDate = get_the_date('', $issue_post_id);

        //Format pub date for front end formatting
        $postDate =  DateTime::createFromFormat('d/m/Y',$postDate);

        $postDate = $postDate->format('Y-m-d');


        // Create the 'article' post and set it's custom fields
        $post_id = wp_insert_post(array (
            'post_type' => 'article',
            'post_title' => (string)$parsed_xml->titlegrp->title,
            'post_status' => 'publish',
            'post_parent'    => $issue_post_id,
            'post_date' => $postDate,
        ));
        if ($post_id == 0){
            return "Failed to create 'article' post for " . $filename . " in " . $folder . "\n";
        }

        // Update ACF fields
        $this->update_acf_field( 'is_issue', "No", $post_id );
        $this->update_acf_field( 'article_type', "nsw_public_health_bulletin_archive", $post_id );
        $this->update_acf_field( 'article_doi', (string)$parsed_xml->pubfront->doi, $post_id );
        $this->update_acf_field( 'article_abstract', (string)$parsed_xml->abstract, $post_id );

        $authors = array();


        foreach($parsed_xml->authgrp->author as $key => $value){

            $newAuthor = array();
            $newAuthor['author_first_name'] = trim((string)$value->name->fname);
            $newAuthor['author_surname'] = trim((string)$value->name->surname);
            $newAuthor['author_name_full'] = $newAuthor['author_first_name'] . ' ' . $newAuthor['author_surname'];

            $authors[] = $newAuthor;
        }

        if(!empty($authors)){
            update_field('field_53fbe9c78cf18', $authors, $post_id );
        }

        //Additional params for citation data
        $pubYear = (string)$parsed_xml->pubfront->year;
        $pubVol = (string)$parsed_xml->pubfront->volno;
        $pages = (string)$parsed_xml->pubfront->fpage . '-' . (string)$parsed_xml->pubfront->lpage;
        $issueVal =(string)$parsed_xml->pubfront->issno;
        $citationData[] = array('citation_key'=> 'VL','citation_value'=> $pubVol);
        $citationData[] = array('citation_key'=> 'SP','citation_value'=> $pages);
        $citationData[] = array('citation_key'=> 'PY','citation_value'=> $pubYear);
        $citationData[] = array('citation_key'=> 'IS','citation_value'=> $issueVal);

        update_field('field_53f6af41fe3c5',$citationData,$post_id);

        // Attach PDF
        if ($pdf_filename){
            $pdf_full_path = $folder."/".$pdf_filename;
            $filetype = wp_check_filetype($pdf_full_path);
            if ($filetype['ext'] != "pdf"){
                return "Failed attaching a non-pdf file " . $pdf_filename . " for " . $filename . " in " . $folder . "\n";
            }
            // Save pdf file into the uploads directory
            $upload = wp_upload_bits($pdf_filename, null, file_get_contents($pdf_full_path));
            if(isset($upload['error']) && $upload['error'] != 0) {
                return ('There was an error uploading your pdf to the upload directory: ' . $upload['error'] . "\n");
            }

            // Prepare an array of post data for the attachment.
            $attachment = array(
                'post_mime_type'	=> $filetype['type'],
                'post_title'     => (string)$parsed_xml->titlegrp->title . "_pdf",
                'post_content'   => '',
                'post_status'    => 'inherit'
            );
            // Insert the attachment.
            $attach_id = wp_insert_attachment( $attachment, $upload['file'], $post_id );
            // Generate and update metadata for the file upload
            if( !function_exists( 'wp_generate_attachment_data' ) )
                require_once(ABSPATH . "wp-admin" . '/includes/image.php');
            $attach_data = wp_generate_attachment_metadata( $attach_id, $pdf_filename );
            wp_update_attachment_metadata( $attach_id,  $attach_data );
            // Update the ACF file field with the PDF attachment
            update_field( $this->get_acfkey('article_pdf'), $attach_id, $post_id );
        }
        return $post_id;
    }

    function existing_issues_migrator() {
        // Prepare error log
        $error_log_file = dirname(__FILE__) . "/error.log";
        $error_handle = fopen($error_log_file, 'a') or die('Cannot open file:  '.$error_log_file);
        date_default_timezone_set('Australia/Melbourne');
        $timestamp = date('m/d/Y h:i:s a');
        fwrite($error_handle, "\n");
        fwrite($error_handle, "--------------------------------------------------------------------". "\n");
        fwrite($error_handle, "Migration started at : ". $timestamp . "\n");
        fwrite($error_handle, "--------------------------------------------------------------------". "\n");

        // Set longer script execution time limit for the migration
        if (!set_time_limit ( 3600 )) {
            fwrite($error_handle, "Failed to increase the script execution time limit". "\n");
        }

        $src_dir = dirname(__FILE__) . '/Test Articles/CSIRO 1 - NSWPHB Normal ISSUES/*';
        // Loop through subdirectory
        $x = 0;
        foreach(glob($src_dir) as $folder)
        {
            echo $folder.'<br><br>';
            echo 'Counter: ' . $x++;

            $rtn_val = $this->add_issue( $folder );
            if (!is_int($rtn_val)) {
                fwrite($error_handle, $rtn_val);
                continue;
            }
            $issue_post_id = $rtn_val;
            echo "\n" . "Issue successfully posted";

            // Issue created. Now create articles.
            $rtn_vals = $this->add_articles($folder, $issue_post_id);
            if ( !array_filter($rtn_vals, 'is_int')) {
                foreach ($rtn_vals as $rtn_val) {
                    if (is_int($rtn_val)) {
                        fwrite($error_handle, $rtn_val);
                    }
                }
            }
        }

        // Close error log
        fclose($error_handle);

        // Reset script execution time limit for the migration
        // set_time_limit ( 30 );
    }

}



new ExistingIssuesMigrator();