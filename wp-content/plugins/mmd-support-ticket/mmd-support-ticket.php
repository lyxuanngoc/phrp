<?php
/**
 * Plugin Name: MMD Support Ticket
 * Plugin URI: https://marameodesign.com
 * Description: Support Ticket
 * Version: 1.0
 * Author: Marameo Design
 **/
function register_mmd_support_ticket_scripts()
{
  $version = '1.0.5';
  wp_register_style('mmd-support-ticket-css', plugin_dir_url(__FILE__) . 'assets/css/style.css');
  //    wp_enqueue_script('mmd-jquery-3.4.1', plugin_dir_url(__FILE__) . 'assets/js/jquery.min.js', ['jquery'], $version);
  if (is_user_logged_in() && current_user_can('administrator')) {
    wp_enqueue_script('mmd-support-ticket-js', 'https://marameodesign.com/wp-content/themes/generatepress_child/js/support-ticket.js', ['jquery'], $version);
  }
}
add_action( 'wp_enqueue_scripts', 'register_mmd_support_ticket_scripts' );
add_action( 'admin_enqueue_scripts', 'register_mmd_support_ticket_scripts' );
add_action( 'admin_enqueue_scripts', 'mmd_get_site_info' );

function mmd_get_site_info() {
  if (is_user_logged_in() && current_user_can( 'manage_options' )) {
    $current_user = wp_get_current_user();
    $site_title = get_bloginfo('name');
    $username = $current_user->display_name;
    $user_email = $current_user->user_email;
    ?>
    <script>
      function getUsername() {
        return '<?php echo $username; ?>'
      }
      function getUserEmal() {
        return '<?php echo $user_email; ?>'
      }
      function getSiteTitle() {
        return '<?php echo $site_title; ?>'
      }
      function getCurrentURL() {
        return window.location.href
      }
      function getClickupURL() {
        return ''
      }
      function getClickupList() {
        return '11208290'
      }
    </script>
    <style>
      body.wp-admin .mmd-support-ticket a{
        top: 270px !important;
      }
      body.wp-admin .mmd-view-all-ticket a,
      body.wp-admin .mmd-support-ticket a{
        text-decoration: none !important;
      }
    </style>
    <?php
  }
}
add_action('wp_footer', 'mmd_get_site_info');
