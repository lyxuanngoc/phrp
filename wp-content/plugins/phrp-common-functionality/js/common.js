jQuery(document).ready(function ($) {

    //submit the search form button for searching on the advanched search page.
    $("#searchFormButton").on('click', function () {

        if ($('#searchform')) {
            $('#searchform').submit();
        }

    });

    //activate tooltips
    $(".phrp_tooltip").tooltip();

    if ($(".datePicker").length > 0)
        $(".datePicker").datepicker();


    /**
     * Let's track file downloads in Google Analytics!
     */
    var filetypes = /\.(zip|exe|pdf|doc*|xls*|ppt*|mp3)$/i;
    var baseHref = '';


    if (jQuery('base').attr('href') != undefined)
        baseHref = jQuery('base').attr('href');


    jQuery('a').each(function () {
        var href = jQuery(this).attr('href');

        if (href && href.match(filetypes)) {

            //On click let's log the GA event and follow through the click
            jQuery(this).on('click', function () {
                var extension = (/[.]/.exec(href)) ? /[^.]+$/.exec(href) : undefined;
                var filePath = href;

                ga('send', 'event', 'FileDownload', extension[0], filePath);

                if (jQuery(this).attr('target') != undefined && jQuery(this).attr('target').toLowerCase() != '_blank') {
                    setTimeout(function () {
                        location.href = baseHref + href;
                    }, 200);
                    return false;
                }
            });
        }

    });

});
