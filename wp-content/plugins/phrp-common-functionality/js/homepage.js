jQuery(document).ready(function ($) {

    //when you toggle between most read or current issue highlights
    $("#toggleArticles").on('click', function () {

        //don't allow user to click the button again until the animation has completed!
        if($("#mostReadArticles:animated").length || $("#currentIssueArticles:animated").length){
            return false;
        }

        //swap text around on the button itself
        var tmpText = $(this).html();
        $(this).html($(this).attr('data-alternative-text'));
        $(this).attr('data-alternative-text', tmpText);

        //swap text around on the H1
        var h1Selector = $("#homepageIssueHeading");
        tmpText = h1Selector.html();
        h1Selector.html(h1Selector.attr('data-alternative-text'));
        h1Selector.attr('data-alternative-text', tmpText);

        //swap the divs for current issue highlights and most read articles
        if ($("#currentIssueArticles").is(":visible")) {

            $("#currentIssueArticles").fadeOut(200, function () {
                jQuery("#mostReadArticles").fadeIn(200)
            });
        }
        else {

            $("#mostReadArticles").fadeOut(200, function () {
                jQuery("#currentIssueArticles").fadeIn(200)
            });
        }

    });

});
