jQuery(document).ready(function ($) {

    $(".show_more").on('click', function () {

        //number of tiles within the CSIRO block.
        //Divide by 12 instead of 6 because there are 12 in a "block" even though there are 6 in a row
        var count = Math.ceil($("#pastIssueCsiro .article-tile").length / 12)  ;

        //height of each tile row
        var tileHeight = $("#pastIssueCsiro .row").height() * 2 ;

        //what's the current height of the whole container
        var currentHeight = $("#pastIssueCsiro").height();

        //If we have more tile rows to show since the current height has not reached the max height
        if (currentHeight < (tileHeight * count)) {

            $("#pastIssueCsiro").animate({height: $("#pastIssueCsiro").height() + tileHeight}, 500, function () {

                if (currentHeight + tileHeight >= (tileHeight * count)) {
                    $(".show_more").html('');
                }
            });
        }


    });


});

