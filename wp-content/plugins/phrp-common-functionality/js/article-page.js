jQuery(document).ready(function ($) {


    //Height of each row in the tag listing.
    var rowHeight = $(".article_tags li:first").innerHeight() + 5;

    //article tags container UL
    var articleTags = $(".article_tags");

    //the view more button
    var viewTopics = $(".viewTopics");


    //Just set the initial height on the topics container to whatever the height is of the first one
    articleTags.height(rowHeight);

    //show to view more button if there's multiple rows of tags
    if( articleTags[0].scrollHeight > rowHeight){

        viewTopics.show(200);

    }

    //when we click the view more button
    viewTopics.on('click', function () {

        var viewMore = $(".viewTopics > i");

        //Animate up or down
        if($(viewMore).hasClass('fa-angle-down')){
            articleTags.animate({height: articleTags[0].scrollHeight}, 500);

        }
        else{
            articleTags.animate({height: rowHeight}, 500);
        }


        //swap the inner text for "View More" and the arrow
        var innerText = $(this).html();

        $(this).html($(this).attr('data-text'));

        $(this).attr('data-text',innerText);

    });


    //Update the plus/minus sign on each panel
    $('.panel').on('show.bs.collapse hide.bs.collapse', function () {

        ($(this).find('h4').toggleClass("heading-collapsed heading-expanded"));

    });

    //Expand all the panels on the article page
    $("#expandPanels").on('click', function () {

        $("[data-toggle='collapse'].collapsed").trigger('click');

    });


    //Collapse all the panels on the article page
    $("#collapsePanels").on('click', function () {

        $("[data-toggle='collapse']:not(.collapsed)").trigger('click');

    });

});
