<?php

/*
Plugin Name: PHRP Common Functionality
Plugin URI: http://4mation.com.au
Description: Declares a plugin that will create common functionality required across the site
Version: 1.0
Author: 4mation
Author URI: http://4mation.com.au
*/

class CommonFormFunctionality extends PluginManagerRegisterBase
{

    private $counter_key = 'post_views_count';
    private $current_issue_uri = '/issue/current-issue/';
    private $online_early_uri = '/issue/online-early/';


    public static function get_filters(){

        return array('request' => 'searchRequestFilter',
        );

}

    public static function get_actions()
    {

        return array(
            'wp_enqueue_scripts' => 'common_scripts',
        );

    }

    public static function get_shortcodes(){

        return array(
            'update_page_counter' => 'update_page_counter',
            'get_page_counter' => 'get_page_counter',
            'redirect_issue_page' => 'redirect_issue_page'
        );

    }

    /**
     * @param $uri
     *
     * This function will check to see if the user is trying to get to the current issue and redirect them automatically
     * This saves us needing the add an extra page template to the current-issue page and instead allows us to just redirect to the
     * URL of the current issue straight away.
     */
    function redirect_issue_page($uri){

        //If you're looking at the current issue URI
        if($uri['uri'] == $this->current_issue_uri){

            //get the current issue CPT based on the ACF field
            $args = array('post_type' => 'article',
                'order' => 'DESC',
                'posts_per_page' => '1',
                'meta_query' => array('relation' => 'AND', array('key' => 'is_issue', 'value' => 'Yes', 'compare' => '==')
                                                             , array('key' => 'issue_type', 'value' => 'Current Issue', 'compare' => '==')));

            $current_issue = get_posts($args);

            //and redirect to it from the ID. Issue a 307 redirect so it's not cached by browsers/search engines since the current issue WILL change
            wp_redirect(get_permalink($current_issue[0]->ID),'307');
        }

        //If you're looking at the online first issue URI
        if($uri['uri'] == $this->online_early_uri){

            //get the current issue CPT based on the ACF field
            $args = array('post_type' => 'article',
                'order' => 'DESC',
                'posts_per_page' => '-1',
                'meta_query' => array(array('key' => 'issue_type', 'value' => 'Online Early', 'compare' => '==')));

            $current_issue = get_posts($args);

            //and redirect to it from the ID. Issue a 307 redirect so it's not cached by browsers/search engines
            wp_redirect(get_permalink($current_issue[0]->ID),'307');
        }



    }

    function common_scripts(){

        //only load this script on the homepage
        if(is_front_page()){
            wp_enqueue_script( 'homepage.js', plugin_dir_url( __FILE__ ) . '/js/homepage.js' , array( 'jquery' ), null, true );
        }

        if(is_page_template('page-template-issue.php')){
            wp_enqueue_script( 'issue-summary-page.js', plugin_dir_url( __FILE__ ) . '/js/issue-summary-page.js' , array( 'jquery' ), null, true );
        }

        if(get_field('is_issue') == 'No') {
            wp_enqueue_script('article-page.js', plugin_dir_url(__FILE__) . '/js/article-page.js', array('jquery'), null, true);

            wp_enqueue_script('add-this.js','//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53e96dc857d301ad',null,true);

        }

        if(is_page_template('page-template-search.php')){
            wp_enqueue_script( 'bootstrap-datepicker.js', get_stylesheet_directory_uri() . '/js/bootstrap-datepicker.js' , array( 'jquery' ), null, true );
            wp_enqueue_style( 'bootstrap-datepicker.css', get_stylesheet_directory_uri() . '/css/bootstrap-datepicker.css' ,array(), null, 'all' );
        }


        //common functionality
        wp_enqueue_script('common.js',plugin_dir_url(__FILE__) . '/js/common.js',array('jquery'),null,true);


    }

    /**
     * @param $post_id
     *
     * Updates the current page counter for $post_id
     */
    function update_page_counter($post_id){

        $post_id = $post_id['post_id'];

            //Returns values of the custom field with the specified key from the specified post.
            $count = get_post_meta($post_id, $this->counter_key, true);

            //If the the Post Custom Field value is empty.
            if($count == ''){
                $count = 0; // set the counter to zero.

                //Delete all custom fields with the specified key from the specified post.
                delete_post_meta($post_id, $this->counter_key);

                //Add a custom (meta) field (Name/value)to the specified post.
                add_post_meta($post_id, $this->counter_key, '1');

                //If the the Post Custom Field value is NOT empty.
            }else{
                $count++; //increment the counter by 1.
                //Update the value of an existing meta key (custom field) for the specified post.
                update_post_meta($post_id, $this->counter_key, $count);

            }
    }

    function get_page_counter($post_id){

        $post_id = $post_id['post_id'];

        return get_post_meta($post_id,$this->counter_key,true);

    }



    /**
     * @param $query_vars
     * @return mixed
     *
     * Force empty string searches to go to the search page
     */
    function searchRequestFilter( $query_vars ) {
        if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
            $query_vars['s'] = " ";
        }
        return $query_vars;
    }

}

new CommonFormFunctionality();
