<?php


class ScholarOneImporterOriginal extends PluginManagerRegisterBase {

  /**
   * Directory within uploads where scholar-one-pdfs will go
   */
  const PDF_DIRECTORY = 'scholar-one-pdfs';

  /**
   * Internal link source.
   *
   * @var string
   */
  const INTERNAL_LINK_SOURCE = 'phrp.com.au';

  /**
   * @var null|string Stores (if any) errors, while injecting PDF
   */
  protected $pdfError = NULL;

  /**
   * @return array Actions
   */
  public static function get_actions() {

    return [
      'admin_enqueue_scripts' => 'enqueue_admin_scripts',
      'admin_menu' => 'register_menu',
      'wp_ajax_zip_upload' => 'zip_upload',
      'wp_ajax_nopriv_zip_upload' => 'zip_upload',
      'wp_ajax_continue_upload' => 'continue_upload',
      'wp_ajax_nopriv_continue_upload' => 'continue_upload',
    ];

  }

  /**
   * Register the menu item that latches onto the function scholar_one_improter
   */
  function register_menu() {
    add_management_page('ScholarOne Issue Importer', 'ScholarOne Issue Importer', 'manage_options', 'issue-importer', [
      $this,
      'scholar_one_importer',
    ]);
  }

  /**
   * Add PDF for article
   * Get the filename from the XML file
   *
   * @param $article SimpleXML object for Article
   * @param $article_id article ID
   * @param $folder directory where the ZIP file was extracted
   *
   * @return bool Whether this function succeeded or not
   */
  public function add_pdf_to_article($article, $article_id, $folder) {
    // Get PDF Directory
    $pdfDir = $folder . DIRECTORY_SEPARATOR . 'pdf_renditions';

    // Get filename for PDF
    $fileName = NULL;
    foreach ($article->file_list->file as $file) {
      foreach ($file->attributes() as $attributeName => $attributeValue) {
        if ($attributeName == 'file_name') {
          $fileName = $attributeValue;
        }
      }
    }

    // If we can't find it, abort
    if ($fileName == NULL) {
      $this->pdfError = 'Cannot find file name in XML';
      error_log($this->pdfError);
      return FALSE;
    }

    // Replace .docx with .pdf - they should have the same filename
    $fileName = str_replace('.docx', '.pdf', $fileName);

    // Match the filename to a pdf
    $pdfFiles = scandir($pdfDir);
    foreach ($pdfFiles as $pdfFile) {
      if (strtolower($pdfFile) == strtolower($fileName)) {
        $fileName = $pdfFile;
        break;
      }
    }

    // Full fule path
    $filePath = $pdfDir . DIRECTORY_SEPARATOR . $fileName;

    // Check file exists on disk
    if (!file_exists($filePath)) {
      $this->pdfError = 'File not found on disk';
      return FALSE;
    }

    // Copy to WP uploads
    $wp_upload_dir = wp_upload_dir();

    $targetFilePath = trailingslashit($wp_upload_dir['basedir'] . DIRECTORY_SEPARATOR . self::PDF_DIRECTORY) . DIRECTORY_SEPARATOR . $fileName;
    copy($filePath, $targetFilePath);

    // Get file type
    $fileType = wp_check_filetype(basename($targetFilePath), NULL);

    // Attachment Data
    $attachment = [
      'guid' => $wp_upload_dir['url'] . '/' . basename($targetFilePath),
      'post_mime_type' => $fileType['type'],
      'post_title' => preg_replace('/\.[^.]+$/', '', basename($targetFilePath)),
      'post_content' => '',
      'post_status' => 'inherit',
    ];

    // Insert the PDF
    $attachmentId = wp_insert_attachment($attachment, $targetFilePath, $article_id);

    // Attach PDF to the article
    $this->update_acf_field('article_pdf', $attachmentId, $article_id);

    return TRUE;
  }

  /**
   * @param $field_name
   * @param $value
   * @param $post_id
   *
   * This is a wrapper function for ACF's update_field function, which:
   * 1. Trims the string
   * 2. Checks for null value
   * Before adding it to the ACF field
   */
  function update_acf_field($field_name, $value, $post_id) {
    $field_key = $this->get_acfkey($field_name);

    if (!is_array($value)) {
      $value = trim($value);
    }

    if ($value) {
      // Only update field if the value is not empty
      update_field($field_key, $value, $post_id);
    }
  }

  /**
   * @param $acfname
   *
   * @return string $acfkey
   *
   * This function takes in a ACF field name (eg: is_issue) and returns the
   *   corresponding ACF field key. It is needed by the ACF update_field()
   *   function
   * (Reference: http://www.advancedcustomfields.com/resources/update_field/)
   */
  function get_acfkey($acfname) {
    $acf_name_to_key_mapping = [
      'is_issue' => 'field_53d1c36fc5b11',
      'issue_type' => 'field_53d1c35ea1579',
      'featured_article' => 'field_53f2b4e1996e2',
      'issue_title_text' => 'field_53d9b54a78b48',
      'issue_inner_text' => 'field_53d9b57078b49',
      'article_type' => 'field_53d1f172393bd',
      'article_doi' => 'field_53f29a8a57ea7',
      'article_pdf' => 'field_53f189ae57aa3',
      'article_abstract' => 'field_53eab30e815a9',
      'article_badge' => 'field_53d5e903da7d0',
      'article_authors' => 'field_53fbe9c78cf18',
      'article_corresponding_author' => 'field_53fc281e2c5eb',
      'article_competing_interests' => 'field_53e9b140e21b9',
      'author_contributions' => 'field_53e9b152e21ba',
      'article_references' => 'field_53eabb176ecda',
      'article_full_text' => 'field_53eae60c33728',
      'article_table_of_contents' => 'field_53eaebb4b54dc',
      'article_key_points' => 'field_53eaeb7353792',
      'article_authors_options' => 'field_53fc017979760',
    ];
    return $acf_name_to_key_mapping[$acfname];
  }

  /**
   * Scholar one importer main page
   */
  function scholar_one_importer() {

    // Check user's permissions
    if (!current_user_can('manage_options')) {
      wp_die(__('You do not have sufficient permissions to access this page.'));
    }

    echo "<h2>" . 'ScholarOne Issue Importer' . "</h2>";
    echo "<p>" . 'Select a zip file of the issue to upload. The importer parses the metadata.xml file and imports each article into WordPress.' . "</p>";
    echo "<p>" . 'Note: The importer assumes a single issue per zip file. The Importer also makes the current Current Issue a Past Issue and makes this issue the new Current Issue' . "</p>";
    echo "<p>" . 'The import process may take a while if the Word documents within the issue are large. Please note that NO images should be embedded within Word documents.';
    ?>

    <div class="wrap">

      <form id="zip_uploader" name="zip_uploader"
            action="/wp-admin/admin-ajax.php?action=zip_upload" method="post"
            enctype="multipart/form-data">
        <div id="uploadContainer">
          <label for="file">Select a file to begin processing:</label>
          <input type="file" name="zip_file_input" id="zip_file_input"><br>
          <div class="import-error"></div>
          <div class="import-success"></div>

          <p class="submit">
            <input type="submit" id="zipUploaderSubmit" name="submit"
                   class="button-primary"
                   value="<?php esc_attr_e('Upload zip file') ?>"/>
          </p>

        </div>
      </form>

    </div>

    <?php

  }

  function enqueue_admin_scripts() {

    wp_enqueue_style('custom.css', plugin_dir_url(__FILE__) . 'css/custom.css');

    wp_enqueue_script('mammoth.js', plugin_dir_url(__FILE__) . '/lib/mammoth.js', ['jquery']);

    wp_enqueue_script('q.js', plugin_dir_url(__FILE__) . '/lib/q.js', ['jquery']);

    wp_enqueue_script('importer.js', plugin_dir_url(__FILE__) . '/js/importer.js', [
      'jquery',
      'mammoth.js',
    ]);


    wp_enqueue_script('jquery-form');

  }

  /**
   * Handles all the ajax requests for uploads
   */
  public function zip_upload() {

    try {
      if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        $uploadDir = wp_upload_dir();

        // Perform checks and then upload zip file
        $file = $_FILES['zip_file_input'];

        if ((empty($_FILES)) || !isset($file)) {
          throw new Exception("No file uploaded. Please upload a .zip file of the issue.");
        }
        $file_name = $file['name'];
        $file_type = wp_check_filetype($file_name);

        if ($file_type['ext'] != 'zip') {
          throw new Exception("Incorrect file format. Please upload a .zip file");
        }
        $upload_overrides = ['test_form' => FALSE];
        $uploaded_file = wp_handle_upload($file, $upload_overrides);

        if (isset($uploaded_file['error'])) {
          throw new Exception("Error uploading zip file: " . $uploaded_file['error']);
        }

        // Unzip the file
        $zip = new ZipArchive;
        $res = $zip->open($uploaded_file['file']);
        if ($res != TRUE) {
          unlink($uploaded_file['file']);
          throw new Exception("Error while unzipping file. Error code: " . $res);
        }
        // Unzip successful. Extract the files to the plugin directory, and delete the .zip file
        // Assume one folder/issue per zip file
        // * $first_sub_folder stores the folder name of the first subfolder within the zip, needed by the parse_metadata()
        //   function
        $first_sub_folder = current(explode("/", $zip->getNameIndex(0)));

        //use basedir to upload the file, then baseurl to read it.
        $tmp_folder = $uploadDir['basedir'] . '/scholar_one/';

        $zip->extractTo($tmp_folder);
        $zip->close();
        unlink($uploaded_file['file']);

        // Begin parsing metadata.xml and creating the issues/articles

        //  $rtn_val = $this->parse_metadata($tmp_folder . $first_sub_folder);

        //get the location of the word documents for parsing
        $filesLocation = $tmp_folder . $first_sub_folder . '/doc/*';

        $_SESSION['scholar_one']['location'] = $tmp_folder . $first_sub_folder;

        $docFiles = [];
        $files = [];

        //Get the names of the doc files and return the for JS parsing in mammoth.
        foreach (glob($filesLocation) as $filePath) {
          $fileName = rawurlencode(basename($filePath));
          // $docFiles[] = $uploadDir['baseurl'] . '/scholar_one/' . $first_sub_folder . '/doc/' . $fileName;
          $docFiles[] = base64_encode(file_get_contents($filePath));
          $files[] = $fileName;
        }
        echo json_encode([
          'success' => TRUE,
          'msg' => $docFiles,
          'filenames' => $files,
        ]);
      }

    } catch (Exception $e) {
      $this->log_error(FALSE, NULL, $e->getMessage());
      echo json_encode(['success' => FALSE, 'msg' => $e->getMessage()]);
    }

    die();
  }

  private function log_error($is_imported, $zip_file = NULL, $err = NULL) {

    $import_id = uniqid();
    $err_log = dirname(__FILE__) . "/error.log";
    $error_handle = fopen($err_log, 'a') or wp_die('Cannot open file:  ' . $err_log);
    date_default_timezone_set('Australia/Melbourne');
    $timestamp = date('m/d/Y h:i:s a');
    $log_init_string = [
      "\n",
      "--------------------------------------------------------------------",
      "Migration started at : " . $timestamp,
      "Import ID : " . $import_id,
      "--------------------------------------------------------------------\n",
    ];
    file_put_contents($err_log, implode("\n", $log_init_string), FILE_APPEND);

    if ($err) {
      if (is_array($err)) {
        file_put_contents($err_log, implode("\n", $err), FILE_APPEND);
      }
      else {
        file_put_contents($err_log, $err . "\n", FILE_APPEND);
      }
    }

  }

  /**
   * Ajax function used to pass in the HTML version of a word DOCX file and to
   * continue the zip file processing.
   */
  function continue_upload() {

    //At this point the zip file is extracted. What sits in the request object should be the HTML documents of the word files.
    //We can simply continue processing the issue and articles.

    if (isset($_SESSION['scholar_one']['location'])) {

      //for now, fudge the array.
      $data = ($_REQUEST['data']);

      $rtn_val = $this->parse_metadata($_SESSION['scholar_one']['location'], $data);

      //delete the scholar one folder that we just imported
      system('/bin/rm -rf ' . escapeshellarg($_SESSION['scholar_one']['location']));

      echo json_encode(['success' => TRUE, 'msg' => 'Issue imported']);

    }
    else {
      echo json_encode([
        'success' => FALSE,
        'msg' => 'Import file does not exist',
      ]);
    }
    die();
  }

  /**
   * @param $folder
   * @param $fullTextArray
   *
   * @return int
   *
   * This function will parse the metadata.xml file, and create the
   *   corresponding issues and articles
   */
  function parse_metadata($folder, $fullTextArray) {

    $rtn_array = [
      'success_post_ids' => [],
      'errors' => [],
    ];

    $metadata_file = $folder . "/metadata.xml";
    if (!file_exists($metadata_file)) {
      array_push($rtn_array['errors'], "metadata.xml not found in: " . $folder);
      return $rtn_array;
    }
    $parsed_xml = simplexml_load_file($metadata_file);

    // Parse Issue Info
    $journal = $parsed_xml->article[0]->journal;

    // Get Issue name and date
    $issueName = $journal->issue_assignment->issue_description;
    $articleAttributes = $parsed_xml->article[0]->attributes();
    $issueExportDate = new DateTime($articleAttributes['export_date']);

    // Create the 'issue' post and set it's custom fields
    $issue_id = wp_insert_post([
      'post_type' => 'article',
      'post_title' => $issueName,
      'post_status' => 'draft',
      // This will set the post date to the issue export date, doesnt seem necessary, but if it ever is;
      // 'post_date'   => $issueExportDate->format('Y-m-d H:i:s'),
    ]);
    if ($issue_id) {
      $this->update_acf_field('is_issue', "Yes", $issue_id);

      // Update the issue title and inner text (shows up on the past-issues page) ACF fields
      $this->update_acf_field('issue_title_text', strtoupper($issueExportDate->format('F Y')), $issue_id);
      $this->update_acf_field('issue_inner_text', $issueName, $issue_id);

      wp_reset_query();  // Restore global post data stored by the_post().
      $this->update_acf_field('issue_type', "Current Issue", $issue_id);
    }

    // Create the post for each articles and set their custom fields
    foreach ($parsed_xml->children() as $article) {
      // Work out which of our parsed filenames matches the actual article we're looking at
      $thisArticleAttributes = $article->attributes();
      $articleFileName = (array) $thisArticleAttributes['ms_no']; # with a .docx on the end, potentially.
      $articleIDMatch = NULL;
      foreach ($fullTextArray['fileNames'] as $fileNameIndex => $fileName) {
        if (substr($fileName, 0, strlen($articleFileName[0])) == $articleFileName[0]) {
          $articleIDMatch = $fileNameIndex;
        }
      }

      $article_id = wp_insert_post([
        'post_type' => 'article',
        'post_title' => $article->article_title,
        'post_status' => 'draft',
        'post_parent' => $issue_id,
      ]);
      if ($article_id) {
        $this->update_acf_field('is_issue', "No", $article_id);
        $this->update_acf_field('article_type', strtolower(str_replace(" ", "_", $article->publication_type)), $article_id);
        $this->update_acf_field('article_abstract', $this->updateLinkTarget($article->abstract), $article_id);
        /* authors */
        $this->parse_author_list($article, $article_id);

        /* doi */
        foreach ($article->article_id_list->children() as $xml_article_id) {
          $attr = $xml_article_id->attributes();
          if (array_key_exists('id_type', $attr) && $attr['id_type'] == 'doi') {
            $this->update_acf_field('article_doi', $xml_article_id, $article_id);
          }
        }
        /* images */
        $images = [];
        foreach ($article->file_list->children() as $file) {
          if (in_array($file->file_extension, ['jpg', 'png', 'gif'])) {
            array_push($images, (string) $file->file_originalname);
          }
        }
        $graphic_folder = $folder . "/graphic";
        $rtn_errs = $this->attach_img_to_article($graphic_folder, $images, $article_id);
        //$rtn_array = array_merge($rtn_array, $rtn_errs);

        /* import docx */
        $this->process_full_text($fullTextArray['results'][$articleIDMatch], $article_id);

      }

    }

    return $rtn_array;
  }

  /**
   * Update all link traces with a target="_blank" if the checked link is an
   * external source.
   *
   * @param string $text
   *
   * @return string
   */
  public function updateLinkTarget($text) {
    $urlPattern = '/<a\s[^>]*href=(\"??)((http|https)[^\" >]*?)\\1[^>]*>(.*)<\/a>/siU';
    $internalLinkSource = self::INTERNAL_LINK_SOURCE;

    $replace = function ($value) use ($internalLinkSource) {
      $link = isset($value[2]) ? $value[2] : '';
      $linkText = isset($value[4]) ? $value[4] : 'link';
      $target = (strpos($link, $internalLinkSource) !== FALSE) ? '' : ' target="_blank"';

      return '<a href="' . $link . '"' . $target . '>' . $linkText . '</a>';
    };

    return preg_replace_callback($urlPattern, $replace, stripslashes($text));
  }

  function parse_author_list($article, $article_id) {

    $article_authors = [];

    foreach ($article->author_list->children() as $author) {
      $attr = $author->attributes();

      $author_name = NULL;
      $corr_author = 'No';

      // author name
      $author_first_name = trim(trim((string) $author->first_name));
      $author_surname = trim(trim((string) $author->last_name));
      $author_name = $author_first_name . ' ' . $author_surname;

      // Is this author also a corresponding author?
      if ((string) $attr['corr'] == "true") {
        $corr_author = 'Yes';
      }

      // author email
      $author_email = NULL;
      if ($author->email) {
        $email_attr = $author->email->attributes();
        if ("primary" == $email_attr['addr_type']) {
          $author_email = (string) $author->email;
        }
      }
      // author affiliation
      $author_affiliation = NULL;
      if ($author->affiliation) {
        $affiliation_attr = $author->affiliation->attributes();
        if ("1" == $affiliation_attr['seq']) {
          $author_affiliation = trim(implode(" ", [
            trim((string) $author->affiliation->inst),
            trim((string) $author->affiliation->dept),
            trim((string) $author->affiliation->city),
            trim((string) $author->affiliation->country),
          ]));
        }
      }
      // field values for the new row in the repeater field
      $author_data_to_insert = [
        'author_email_address' => $author_email,
        'author_affiliations' => $author_affiliation,
        "author_name_full" => $author_name,
        'author_first_name' => $author_first_name,
        'author_surname' => $author_surname,
        'corresponding_author' => $corr_author,
      ];


      $article_authors[] = $author_data_to_insert;

    }

    $this->update_acf_field('article_authors', $article_authors, $article_id);
  }

  function attach_img_to_article($graphic_folder, $images, $article_id) {
    $rtn_array = [
      'success_post_ids' => [],
      'errors' => [],
    ];

    foreach ($images as $image) {
      $image_fullpath = $graphic_folder . '/' . $image;

      $filetype = wp_check_filetype(basename($image_fullpath), NULL);
      $wp_upload_dir = wp_upload_dir();

      // Save image into the uploads directory
      $upload = wp_upload_bits(basename($image_fullpath), NULL, file_get_contents($image_fullpath));
      if (isset($upload['error']) && $upload['error'] != 0) {
        array_push($rtn_array['errors'], 'There was an error uploading image ' . $image . ' into the upload directory: ' . $upload['error']);
        return $rtn_array;
      }

      $attachment = [
        'guid' => $wp_upload_dir['url'] . '/' . basename($image_fullpath),
        'post_mime_type' => $filetype['type'],
        'post_title' => preg_replace('/\.[^.]+$/', '', basename($image_fullpath)),
        'post_content' => '',
        'post_status' => 'inherit',
      ];

      $attachment_id = wp_insert_attachment($attachment, $upload['file'], $article_id);
      // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
      require_once(ABSPATH . 'wp-admin/includes/image.php');

      $attach_data = wp_generate_attachment_metadata($attachment_id, $upload['file']);
      wp_update_attachment_metadata($attachment_id, $attach_data);
    }
  }

  /**
   * @param $text
   * @param $article_id
   * Takes the full text and an article id and adds it to that article.
   */
  function process_full_text($text, $article_id) {
    //Take the full text, strip out the <toc> tags to become the table of contents and create the link.

    //convert to HTML characters.
    $text = html_entity_decode($text);
    $text = $this->updateLinkTarget($text);

    //match the TOC tags within H2 and TOC tags.
    preg_match_all('/<toc>(.*?)<\/toc>/', $text, $match);

    //store the matches and the text
    $matches = $match[0];
    $matchesText = $match[1];

    $tocArray = [];

    //foreach TOC match, create a TOC entry and replace the text with the new link.
    foreach ($matches as $key => $value) {
      // Text matched as a TOC
      $matchText = strip_tags($matchesText[$key]);

      $tocItem = [];

      $headingId = 'TocEntry' . $key;

      $tocItem['item_text'] = $matchText;
      $tocItem['item_link'] = $headingId;

      $tocArray[] = $tocItem;

      $newHeading = '<h2 id="' . $headingId . '">' . $matchText . '</h2>';

      //Replace the input heading with our new one that has the ID.
      $text = str_replace($value, $newHeading, $text);
    }

    //Add the table of contents

    $this->update_acf_field("article_table_of_contents", $tocArray, $article_id);

    //Take out the <reference> tags and put them in the reference fields.

    preg_match_all('/<reference>(.*?)<\/reference>/', $text, $referenceMatches);

    $referenceMatch = $referenceMatches[0];
    $referenceMatchText = $referenceMatches[1];

    $referenceArray = [];

    foreach ($referenceMatch as $key => $value) {

      $referenceItem = [];

      $referenceItem['reference'] = $referenceMatchText[$key];

      $referenceArray[] = $referenceItem;

      $text = str_replace($value, '', $text);
    }

    //Replace references heading with nothing.
    $text = str_replace('<p>References</p>', '', $text);


    $this->update_acf_field('article_references', $referenceArray, $article_id);


    $text = $this->updateReferenceTags($text);

    //Add in the full text with adjusted data
    $this->update_acf_field("article_full_text", $text, $article_id);

  }

  /**
   * @param $text
   *
   * @return mixed
   *
   * Takes the full text for an article, locates the sup tags and replaces them
   *   with anchor tags to the reference section.
   */
  private function updateReferenceTags($text) {

    preg_match_all('/<sup>(.*?)<\/sup>/', $text, $supMatches);

    $supMatch = $supMatches[0];
    $supMatchText = $supMatches[1];

    $supArray = [];

    foreach ($supMatch as $key => $value) {

      //supMatch contains the full <sup>1</sup>
      //supMatchText contains just the 1

      $matchedText = $supMatchText[$key];

      if (strpos($matchedText, ',') > -1) {
        //Multi reference match

        //let's make an array of these references
        $references = explode(',', str_replace(' ', '', $matchedText));
        $newLink = '<sup>';
        foreach ($references as $refKey => $reference) {
          $newLink .= '<a href="#refList' . $reference . '">' . $reference . '</a>';
          if ($refKey !== (count($references) - 1)) {
            $newLink .= ',';
          }
        }
        $newLink .= '</sup>';
      }
      elseif (strpos($matchedText, '-') > -1) {
        //Multi reference match however we'll only use the first one
        $beforeHypen = strstr($matchedText, '-', TRUE);
        $newLink = '<sup><a href="#refList' . $beforeHypen . '">' . $matchedText . '</a></sup>';
      }
      else {
        //Single reference match
        $newLink = '<sup><a href="#refList' . $matchedText . '">' . $matchedText . '</a></sup>';
      }

      //replace the reference with the new link
      $text = str_replace($supMatch[$key], $newLink, $text);
    }

    return $text;

  }

}

new ScholarOneImporterOriginal();

// Laravel's dd
if (!function_exists('dd')) {
  function dd($var) {
    var_dump($var);
    die;
  }
}
