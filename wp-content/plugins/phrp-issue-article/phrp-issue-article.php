<?php

/*
Plugin Name: PHRP Issues and Articles
Plugin URI: http://4mation.com.au
Description: Declares a plugin that will create Issues and Articles CPTs for use in the PHRP site
Version: 1.0
Author: 4mation
Author URI: http://4mation.com.au
*/

class IssueArticle extends PluginManagerRegisterBase
{

    //Article type information to be used throughout the site
    public static $articleTypes = array(
        'perspectives' => array('name'=>'Perspectives',
            'order' => '1',
            'color' => 'btn-blue',
            'searchable' => true),
        'research' => array('name'=>'Research',
            'order' => '2',
            'color' => 'btn-green',
            'searchable' => true),
        'in_practice' => array('name'=>'In Practice',
            'order' => '3',
            'color' => 'btn-purple',
            'searchable' => true),
        'brief_reports' => array('name'=>'Brief Reports',
            'order' => '4',
            'color' => 'btn-blueLight',
            'searchable' => true),
        'news_and_views' => array('name'=>'News And Views',
            'order' => '5',
            'color' => 'btn-greenMediumDark',
            'searchable' => true),
        'online_early' => array('name'=>'Online Early',
            'order' => '6',
            'color' => 'btn-greyDark',
            'searchable' => true),
        'nsw_public_health_bulletin_archive' => array('name' => 'NSW Public Health Bulletin archive',
            'order' => '7',
            'color' => 'btn-grayLighter',
            'searchable' => false),

    );

    public static function get_actions()
    {

        return array(
            'init' => 'register_funcs',
            'admin_enqueue_scripts' => 'admin_scripts',
            'manage_article_posts_custom_column' => array('manage_admin_columns_data',10,2),
            'pre_get_posts' => 'nwtd_lpfs_custom_admin_query',

        );

    }

    function nwtd_lpfs_custom_admin_query( $query ) {

        if( !is_admin() && !$query->is_main_query() ) {
            return;
        }

        if( is_post_type_archive( 'article' ) ) {
            $query->set('no_found_rows', 1 );
            $query->set('update_post_meta_cache', 0 );
            $query->set('update_post_term_cache', 0 );
        }
    }
    public static function get_filters(){

        return array('manage_edit-article_columns' => 'manage_admin_columns',
            'posts_where' => array('custom_where',10,2),);
    }

    /**
     *
     * Removes the ability to select a parent topic for topics
     */
    function admin_scripts($hook)
    {
        global $typenow;

        //only run this script if we're either in the tag editing screen for topics, or if we're looking at the article CPT
        if (!empty($_GET['taxonomy']) && (!( in_array($_GET['taxonomy'],array('topics')) || $typenow == 'article' )))
            return;

        wp_enqueue_script('issue_article',plugin_dir_url( __FILE__ )  . '/scripts/issue_article.js', array('jquery'));

    }

    /**
     * Add some additional column data for this CPT
     */
    function manage_admin_columns_data($column, $post_id){

        switch($column){
            case 'Is An Issue':
                $field = get_field('is_issue',$post_id);
                echo $field;
                break;
            case 'Issue Type':
                $field = get_field('issue_type',$post_id);
                echo $field;
                break;

        }

    }

    /**
     * Add some additional columns for this CPT in the admin section
     */
    function manage_admin_columns($columns){

        $newColumns = array('Is An Issue' => __('Is An Issue','phrp'),
            'Issue Type' => __('Issue Type','phrp'));
        return array_merge($columns,$newColumns);

    }

    function register_funcs(){

        $this->register_post_types();
        $this->register_taxonomies();
    }

    function register_post_types()
    {

        // Articles
        $labels = array(
            'name' => _x('Articles', 'post type general name'),
            'singular_name' => _x('Article', 'post type singular name'),
            'add_new' => _x('Add New', 'Article'),
            'add_new_item' => __('Add New Article'),
            'edit_item' => __('Edit Article'),
            'new_item' => __('New Article'),
            'all_items' => __('All Articles'),
            'view_item' => __('View Article'),
            'search_items' => __('Search Articles'),
            'not_found' => __('No Articles found'),
            'not_found_in_trash' => __('No Articles found in Trash'),
            'parent_item_colon' => '',
            'menu_name' => __('Articles')
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'taxonomies' => array( 'post_tag','topics'),
            'query_var' => true,
            'capability_type' => 'page',
            'rewrite' => array('slug' => 'issues', 'with_front' => false),
            'has_archive' => true,
            'hierarchical' => true,
            'taxonomies' => array('status'),
            'menu_icon' => get_stylesheet_directory_uri() . '/img/book-arrow.png',
            'supports' => array('title', 'editor', 'page-attributes')
        );
        register_post_type('article', $args);

    }

    function register_taxonomies(){

        //register custom taxonomy 'topics' for articles CPT.
        $labels = array(
            'name' => _x( 'Topics', 'taxonomy general name' ),
            'singular_name' => _x( 'Topic', 'taxonomy singular name' ),
            'search_items' =>  __( 'Search Topics' ),
            'all_items' => __( 'All Topics' ),
            'parent_item' => __( 'Parent Topic' ),
            'parent_item_colon' => __( 'Parent Topic:' ),
            'edit_item' => __( 'Edit Topic' ),
            'update_item' => __( 'Update Topic' ),
            'add_new_item' => __( 'Add New Topic' ),
            'new_item_name' => __( 'New Topic Name' ),
            'menu_name' => __( 'Topics' ),
        );


        register_taxonomy('topics',array('article'), array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'topics' ),
        ));


    }

    /**
     * @param $where
     * @return mixed
     *
     * Override the where clause when doing a WPQuery.
     */
    function custom_where( $where, $wp_query )
    {

        /**
         * If you're running an advanced search you can search the post title of articles. We can't use the "s" parameter
         * since the 'search everything' plugin will latch onto this and then search all the pages/posts which we don't want.
         * Instead we set a custom variable advanced_search_post_title and modify the query directly.
         */
        if($wp_query->get('advanced_search_post_title') && $wp_query->get('advanced_search_post_title') != ''){
            global $wpdb;

            $where .= " AND " . $wpdb->posts . ".post_title LIKE '%" . esc_sql(like_escape($wp_query->get('advanced_search_post_title'))) . "%'";
        }

        /**
         * This is for when you are searching based on the article author field, since we don't know what the field name is for the
         * n number of authors we have to use a like query.
         */
        if(strpos($where,'article_authors_%_author_name') != -1){

            $where = str_replace("meta_key = 'article_authors_%_author_name'", "meta_key LIKE 'article_authors_%_'", $where);

        }

        return $where;
    }


    /**
     * @param $article
     * @param $currentArticleType
     * @param $displayType
     *
     * Just a placeholder to output the article container since we use it in several places
     */
    public static function article_snippet($article,$currentArticleType,$displayType = 'default'){

        $currentArticleTypeData = get_field_object('article_type');

        //get the label for display purposes
        $articleLabel = isset($currentArticleTypeData['choices'][$currentArticleType]) ? strtoupper($currentArticleTypeData['choices'][$currentArticleType]) : '';

        //Get the authors that are associated with this article
        $articleAuthors = get_field('article_authors',$article->ID);

        $articleAuthorNames = array();

        if($articleAuthors){

            foreach ($articleAuthors as $author)
                $articleAuthorNames[] = $author['author_name_full'];
        }

        ?>
        <div class="article_snippet <?php echo $currentArticleType; ?>">
            <div class="row">
                <div class="col-xs-12 col-md-10">
                    <a href="<?php echo get_permalink($article->ID); ?>" class="article_snippet_body">
                        <?php 
                            $isThemed = get_field('is_themed',$article->ID);
                            if($isThemed):
                        ?>
                        <div class="flag-themed">THEMED</div>
                        <?php endif; ?>
                        <h5 class="article_title"><?php echo get_the_title($article->ID); ?></h5>

                        <p class="author"><?php echo implode(', ', $articleAuthorNames); ?></p>

                        <p class="date"><?php echo 'Published  ' . get_the_date('j F Y', $article->ID); ?></p>

                        <span>Read more <i
                                class="fa fa-angle-right"></i>
                            </span>
                    </a>
                </div>

                <?php if (!in_array($currentArticleType, array('other'))) { ?>
                    <div class="col-xs-12 col-md-2">

                        <?php
                        if ($currentArticleType != 'nsw_public_health_bulletin_archive' && $displayType == 'search') {
                            ?>
                            <div
                                class="btn <?php echo IssueArticle::$articleTypes[$currentArticleType]['color']; ?> outline"><?php echo $articleLabel; ?></div>
                        <?php }
                        ?>
                        <ul class="list-unstyled article_preview_icon">
                            <?php if(get_field('article_abstract',$article->ID)){ ?>
                                <li>
                                    <a href="<?php echo get_permalink($article->ID).'#abstractPanel'; ?>" class="abstract btn-block">
                                        <i class="fa fa-align-left"></i>
                                        <span class="hidden-xs hidden-sm">View abstract</span>
                                    </a>
                                </li>
                            <?php } ?>

                            <?php if(get_field('article_full_text',$article->ID)){ ?>
                                <li>
                                    <a href="<?php echo get_permalink($article->ID).'#fullTextPanel'; ?>" class="full_text">
                                        <i class="fa fa-book"></i>
                                        <span class="hidden-xs hidden-sm">View full text</span>
                                    </a>
                                </li>
                            <?php }

                            $articlePdf = get_field('article_pdf',$article->ID);

                            if($articlePdf) { ?>
                                <li>
                                    <a href="<?php echo $articlePdf['url']; ?>" class="pdf" title="Download PDF" target="_blank">
                                        <i class="fa fa-file-pdf-o"></i>
                                        <span class="hidden-xs hidden-sm">View PDF</span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>
        </div>

    <?php }

}

new IssueArticle();
