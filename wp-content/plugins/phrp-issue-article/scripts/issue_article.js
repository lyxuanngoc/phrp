/**
 * This will hide the parent option for a custom taxonomy as well as removing the "add new" on the edit/new CPT screen
 */
jQuery(document).ready(function ($) {

    if (window.location.href.indexOf('action') > -1) {
        $('label[for=parent]').parent().parent().remove();
    }
    else {
        $('label[for=parent]').parent().remove();
    }

    $("#newtopics_parent").remove();
    $("#newarticleauthors_parent").remove();
});
