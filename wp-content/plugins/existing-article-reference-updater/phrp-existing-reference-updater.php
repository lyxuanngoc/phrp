<?php

/*
Plugin Name: PHRP Existing article reference updater
Plugin URI: http://4mation.com.au
Description: Declares a plugin to update article reference links
Version: 1.0
Author: 4mation
Author URI: http://4mation.com.au
*/

class ReferenceUpdater extends PluginManagerRegisterBase
{

    public static function get_actions()
    {

        return array(
            'admin_menu' => 'register_menu',
        );

    }

    function register_menu()
    {
        add_management_page('Reference Updater', 'Reference Updater', 'manage_options', basename(__FILE__), array($this, 'reference_updater_entry'));
    }


    function reference_updater_entry()
    {

        $args = array(
            'numberposts' => -1,
            'posts_per_page' => -1,
            'post_type' => 'article',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'is_issue',
                    'value' => 'No',
                    'compare' => '=='
                )
            )
        );

        $wp_query = new WP_Query($args);

        if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();

            $id = $wp_query->post->ID;

            //now we have the full text for this article. Let's find all the <sup> tags.


            $fullText = get_field('article_full_text');

            preg_match_all('/<sup>(.*?)<\/sup>/', $fullText, $supMatches);

            $supMatch = $supMatches[0];
            $supMatchText = $supMatches[1];

            $supArray = array();

            foreach ($supMatch as $key => $value) {

                //supMatch contains the full <sup>1</sup>
                //supMatchText contains just the 1

                $matchedText = $supMatchText[$key];

                if (strpos($matchedText, ',') > -1) {
                    //Multi reference match

                    //let's make an array of these references
                    $references = explode(',', str_replace(' ', '', $matchedText));
                    $newLink = '<sup>';
                    foreach ($references as $refKey => $reference) {



                        if ((strpos($reference, '-') > -1)) {
                            //Multi reference match however we'll only use the first one
                            $beforeHyphen = strstr($reference, '-', true);

                            $newLink .= '<a href="#refList' . $beforeHyphen . '">' . $reference . '</a>';
                        } elseif ((strpos($reference, '–') > -1)) {
                            //Multi reference match however we'll only use the first one
                            $beforeHyphen = strstr($reference, '–', true);

                            $newLink .= '<a href="#refList' . $beforeHyphen . '">' . $reference . '</a>';
                        }else {
                            $newLink .= '<a href="#refList' . $reference . '">' . $reference . '</a>';
                        }



                        if ($refKey !== (count($references) - 1)) $newLink .= ',';
                    }
                    $newLink .= '</sup>';
                } elseif ((strpos($matchedText, '-') > -1)) {
                    //Multi reference match however we'll only use the first one
                    $beforeHyphen = strstr($matchedText, '-', true);

                    $newLink = '<sup><a href="#refList' . $beforeHyphen . '">' . $matchedText . '</a></sup>';
                } elseif ((strpos($matchedText, '–') > -1)) {
                    //Multi reference match however we'll only use the first one
                    $beforeHyphen = strstr($matchedText, '–', true);

                    $newLink = '<sup><a href="#refList' . $beforeHyphen . '">' . $matchedText . '</a></sup>';
                } else {
                    //Single reference match
                    $newLink = '<sup><a href="#refList' . $matchedText . '">' . $matchedText . '</a></sup>';
                }

                //replace the reference with the new link
                $fullText = str_replace($supMatch[$key], $newLink, $fullText);
            }

            update_field('article_full_text', $fullText, $id);

        endwhile;
        endif;
    }

}


new ReferenceUpdater();