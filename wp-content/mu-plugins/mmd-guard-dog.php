<?php

/**
 * Plugin Name: [Marameo Design] Guard-dog
 * Plugin URI: https://marameodesign.com
 * Description: Enable dev modes for non-prod envs
 * Version: 1.0
 * Author: Marameo Design
 **/

define('LIVE_SERVER_NAME_MD5', '4d0ea30574d26d4b49e7a529d6a4185d'); //md5('www.phrp.com.au')

if (md5($_SERVER['SERVER_NAME']) != LIVE_SERVER_NAME_MD5) {
  // Disable WP cron
  define('DISABLE_WP_CRON', true);

  // Mark as staging for llms
  define('LLMS_SITE_IS_CLONE', true);
  define('LLMS_SITE_FEATURE_RECURRING_PAYMENTS', false);

  // Mark as staging for woo subscription
  add_filter('woocommerce_subscriptions_is_duplicate_site', '__return_true');
  define( 'WCPAY_DEV_MODE', true );

  // Discourage search engines
  add_action('pre_option_blog_public', '__return_zero');

  // Reroute email
  function mmd_modify_mail_message($mail_parts) {
    $mail_parts['to'] = 'mm.phrp@yopmail.com';
    return $mail_parts;
  }
  add_filter('wp_mail', 'mmd_modify_mail_message', 1000, 1);
}
