<?php

/**
 * Plugin Name: Grace Core Functions
 * Plugin URI: NA
 * Description: Core Grace Group functions that are needed on all sites
 * Author: 4mation
 * Author URI: http://4mation.com.au
 * Version: 0.1.0
 */

/* Place custom code below this line. */


/**
 *  Interface required for all Grace made plugins, load it here just once so it's available since mu-plugins are loaded
 *  before plugins.
*/

$baseURL = dirname( __FILE__ );

require_once($baseURL.'/classes/plugin-interfaces.php');

/**
 * Base class required for all Grace plugins
 */
require_once($baseURL.'/classes/plugin-base-class.php');

/**
 * The Grace plugin manager instance
 */

require_once($baseURL.'/classes/plugin-manager.php');

