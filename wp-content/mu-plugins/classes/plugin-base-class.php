<?php

/**
 * Plugin base class. Simply registers itself to the plugin manager instance
 */

abstract class PluginManagerRegisterBase Implements Plugin_SubscriberInterface{

    /**
     * Get instance of the plugin manager and add yourself to it
     */
    function __construct(){

        GLOBAL $_pluginManager;

       if($_pluginManager instanceof WP_Plugin_API_Manager){

            $_pluginManager->register($this);

       }

    }


    public static function get_actions()
    {

        return array();

    }

    public static function get_filters()
    {

        return array();

    }

    public static function get_shortcodes(){

        return array();

    }


}