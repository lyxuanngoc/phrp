<?php

/**
 * WP_Plugin_API_Manager handles registering actions and hooks with the
 * WordPress Plugin API.
 */
class WP_Plugin_API_Manager
{
    /**
     * Registers an object with the WordPress Plugin API.
     *
     * @param mixed $object
     */
    public function register($object)
    {
        if ($object instanceof Plugin_SubscriberInterface) {

            $this->register_actions($object);

            $this->register_filters($object);

            $this->register_shortcodes($object);
        }
    }

    /**
     * Register an object with a specific action hook.
     *
     * @param Plugin_SubscriberInterface $object
     * @param string                          $name
     * @param mixed                           $parameters
     */
    private function register_action(Plugin_SubscriberInterface $object, $name, $parameters)
    {
        if (is_string($parameters)) {
            add_action($name, array($object, $parameters));
        } elseif (is_array($parameters) && isset($parameters[0])) {
            add_action($name, array($object, $parameters[0]), isset($parameters[1]) ? $parameters[1] : 10, isset($parameters[2]) ? $parameters[2] : 1);
        }
    }

    /**
     * Regiters an object with all its action hooks.
     *
     * @param Plugin_SubscriberInterface $object
     */
    private function register_actions(Plugin_SubscriberInterface $object)
    {
        foreach ($object->get_actions() as $name => $parameters) {
            $this->register_action($object, $name, $parameters);
        }
    }

    /**
     * Register an object with a specific shortcode hook.
     *
     * @param Plugin_SubscriberInterface $object
     * @param string                          $name
     * @param mixed                           $parameters
     */
    private function register_shortcode(Plugin_SubscriberInterface $object, $name, $parameters)
    {

        if (is_string($parameters)) {
            add_shortcode($name, array($object, $parameters));
        }
    }

    /**
     * Regiters an object with all its shortcode hooks.
     *
     * @param Plugin_SubscriberInterface $object
     */
    private function register_shortcodes(Plugin_SubscriberInterface $object)
    {
        foreach ($object->get_shortcodes() as $name => $parameters) {
            $this->register_shortcode($object, $name, $parameters);
        }
    }
    /**
     * Register an object with a specific filter hook.
     *
     * @param Plugin_SubscriberInterface $object
     * @param string                          $name
     * @param mixed                           $parameters
     */
    private function register_filter(Plugin_SubscriberInterface $object, $name, $parameters)
    {

        if (is_string($parameters)) {
            add_filter($name, array($object, $parameters));
        } elseif (is_array($parameters) && isset($parameters[0])) {
            add_filter($name, array($object, $parameters[0]), isset($parameters[1]) ? $parameters[1] : 10, isset($parameters[2]) ? $parameters[2] : 1);
        }
    }

    /**
     * Regiters an object with all its filter hooks.
     *
     * @param Plugin_SubscriberInterface $object
     */
    private function register_filters(Plugin_SubscriberInterface $object)
    {
        foreach ($object->get_filters() as $name => $parameters) {
            $this->register_filter($object, $name, $parameters);
        }
    }
}

GLOBAL $_pluginManager;

$_pluginManager = new WP_Plugin_API_Manager();
